<div class="row">
    <div class="col s12 center-align"><h4><b>Introduction</b></h4></div>
    <div class="col s12 l10 offset-l1">
        <p>The site runs in <b>Windows</b> (Operating System), and is powered by <b>Apache</b> (Web Server), 
        <b>MySQL</b> (Database), <b>PHP</b> (Programming Language) and some <b>JavaScript</b> libraries for the frontend.
        Together they form the <b>WAMP Stack</b></p>

        <h6><b>Apache</b></h6>
        <p>The Apache web server processes requests and serves up web assets via HTTP so that the application is accessible to 
        anyone in the public domain over a simple web URL. Developed and maintained by an open community, Apache is a mature, 
        feature-rich server that runs a large share of the websites currently on the internet. </p>

        <h6><b>MySQL</b></h6>
        <p>MySQL is an open source relational database management system for storing application data. With My SQL, you can store 
        all your information in a format that is easily queried with the SQL language. SQL is a great choice if you are dealing with
        a business domain that is well structured, and you want to translate that structure into the backend. MySQL is suitable for 
        running even large and complex sites.</p>

        <h6><b>PHP</b></h6>
        <p>The PHP open source scripting language works with Apache to help you create dynamic web pages. You cannot use HTML to perform 
        dynamic processes such as pulling data out of a database. To provide this type of functionality, you simply drop PHP code into the 
        parts of a page that you want to be dynamic.</p>

        <h6><b>How the elements work together</b></h6>
        <p>A high-level look at the WAMP stack order of execution shows how the elements interoperate. The process starts when the Apache web 
        server receives requests for web pages from a user’s browser. If the request is for a PHP file, Apache passes the request to PHP, which 
        loads the file and executes the code contained in the file. PHP also communicates with MySQL to fetch any data referenced in the code. 
        <br><br>
        PHP then uses the code in the file and the data from the database to create the HTML that browsers require to display web pages. The WAMP stack is 
        efficient at handling not only static web pages, but also dynamic pages where the content may change each time it is loaded depending on the date, time, 
        user identity and other factors. 
        <br><br>
        After running the file code, PHP then passes the resulting data back to the Apache web server to send to the browser. It can also store this new data 
        in MySQL. And of course, all of these operations are enabled by the Windows operating system running at the base of the stack.</p>
    </div>
    <div class="col s12 l10 offset-l1">
        <img src="/tienda/assets/img/wamp-stack.png" alt="wamp-stack">
    </div>
</div>