<?php
    if (isset($subCategorias)) : 
?>

        <center><h5><b>SUBCATEGORIES</b></h5></center>

<?php
        if (count($subCategorias) > 0):
?>
            <div class="collection category-explorer">

<?php 
            $idSubcategory = isset($_GET['idSubcategoria']) ? $_GET['idSubcategoria'] : '';
            foreach ($subCategorias as $sub):
                $SACTIVE =  ($sub->id_subcategoria === $idSubcategory) ? 'active': '';
?>
                <a href="/tienda/store/category.php?idCategoria=<?= $sub->id_categoria ?>&idSubcategoria=<?= $sub->id_subcategoria ?>" class="collection-item black-text <?= $SACTIVE ?>"><?= strtoupper($sub->subcategoria) ?></a>
<?php 
            endforeach;
        else :
?>
            <a href="#" class="collection-item grey-text">No subcategories found</a>

<?php
        endif;
?>

</div>

<?php 
    endif; 
?>