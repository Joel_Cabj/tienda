<header>
    <div class="navbar-fixed">
        <nav>
        <div class="nav-wrapper">
            <div class="container header">
                <div class="row">
                    <div class="col s12">
                        <a href="#" data-target="sidenav-menu" class="left sidenav-trigger hide-on-medium-and-up"><i class="material-icons nav-icon">menu</i></a>
                        <a href="/tienda" class="brand-logo hide-on-small-only"><img class="svg-logo" src="<?= $ASSETS ?>img/logo_black.svg" alt="Corsair"></a>
                        <a href="/tienda" class="brand-logo hide-on-med-and-up"><img class="svg-logo-res" src="<?= $ASSETS ?>img/logo_mobile_black.svg" alt="Cosair"></a>
                        <a href="#" data-target="sidenav-user" class="right sidenav-trigger show-on-medium-and-up"><i class="material-icons nav-icon">shopping_cart</i></a>
                        <form method="GET" action="/tienda/store/">
                            <div class="input-field right hide-on-med-and-down">
                                <input id="search" name="search" type="search" class="search-bar" required>
                                <label class="label-icon" for="search"><i class="material-icons nav-icon">search</i></label>
                                <i class="material-icons">close</i>
                            </div>
                        </form>
                        <?php require_once($COMPONENTS."/user/navmenu.component.php") ?>
                    </div>
                </div>
            </div>			
        </div> 
        </nav>
    </div>

    <div class="progress" id="progressBarQuery" style="margin: 0px; visibility: hidden;">
        <div class="indeterminate"></div>
    </div>
</header>