<div class="category-banner">
    <?php if ($categoria->categoria != ''): ?>
        <h2 class="category-banner-title"><?= strtoupper($categoria->categoria) ?></h2>
    <?php else: ?>
        <h2 class="category-banner-title">HOT PRODUCTS</h2>
    <?php endif; ?>
</div>