<main>
    <?php require($COMPONENTS."store/banner.component.php") ?>
    <div class="row">
        <div class="col l3 hide-on-med-and-down">
            <?php require($COMPONENTS."store/category-menu.component.php") ?>
            <?php require($COMPONENTS."store/subcategory.component.php") ?>
        </div>
        <div class="col l9">
            <?php require($COMPONENTS."store/product.component.php") ?>
        </div>
    </div>
</main>