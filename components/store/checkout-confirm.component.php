<div class="row">
    <div class="col l6 s12">
        <div class="row">
            <center><h4><b>ADDITIONAL INFO</b></h4></center>
            <br>
            <div class="col s8 offset-s2 hoverable">
                <div class="input-field">
                    <input value="<?= $user->direccion?>" id="direccion" name="direccion" type="text" require>
                    <label for="direccion">Address</label>
                </div>
            </div>
            <div class="col s4 offset-s2 hoverable">
                <div class="input-field">
                    <input value="<?= $user->ciudad?>" id="city" name="ciudad" type="text" require>
                    <label for="city">City</label>
                </div>
            </div>
            <div class="col s4 hoverable">
                <div class="input-field">
                    <input value="<?= $user->provincia?>" id="state" name="provincia" type="text" require>
                    <label for="state">State</label>
                </div>
            </div>
            <br>
        </div>
    </div>

    <div class="col l6 s12" style="margin-bottom: 50px;">
        <div class="row">
            <center><h4><b>CART OVERVIEW</b></h4></center>
            <br>
            <div class="cart-overview-section"></div>

            <div class="col s8 offset-s2 black white-text total_price_col">
                <div class="col s6 left-align">
                    <h5><b>Total Price</b></h5>
                </div>
                <div class="col s6 right-align">
                    <h5><b class="total_price">$</b></h5>
                    <span class="helper-text white-text">Quantity: <b class="items_quantity">#</b></span><br>
                </div>
            </div>
        </div>
    </div>
           
    <div class="col s6 right-align">
        <a class="btn waves-effect waves-light white black-text btn_back"><i class="material-icons left">navigate_before</i>BACK</a>           
    </div>    
    <div class="col s6 left-align">
        <button class="btn btn_confirm waves-effect waves-light yellow accent-4 black-text" type="submit" name="action"><i class="material-icons right">check</i>CONFIRM</button>           
    </div>              
</div>