<center><h5><b>CATEGORIES</b></h5></center>
<div class="collection category-explorer">

<?php 
    if (count($categorias) > 0) : 
        $idCategory = isset($_GET['idCategoria']) ? $_GET['idCategoria'] : '';
        $HP =  ($idCategory === '') ? 'active': '';
?>
        <a href="/tienda/store/" class="collection-item black-text <?= $HP ?>">HOT PRODUCTS</a>
<?php
        foreach ($categorias as $cat):
            $ACTIVE =  ($idCategory === $cat->id_categoria) ? 'active': '';
?>
            <a href="/tienda/store/category.php?idCategoria=<?= $cat->id_categoria ?>" class="collection-item black-text <?= $ACTIVE ?>"><?= strtoupper($cat->categoria) ?></a>
<?php 
        endforeach; 
    else:
?>
        <a href="/tienda/store/" class="collection-item black-text">HOT PRODUCTS</a>
        <a href="#" class="collection-item grey-text">No categories found</a>
<?php 
    endif; 
?>

</div>
