<div class="row">
    <?php if ( count($productos) > 0) : ?>
        <?php foreach($productos as $producto) : ?>
            <div class="col l4 m6 s12">
                <div class="card hoverable sticky-action">
                    <div class="card-image waves-effect waves-block waves-light">
                        <?php if (isset($producto->img)) : ?>
                            <img class="activator" src="<?= $ASSETS ?>/img/product/uploads/<?= $producto->img ?>">
                        <?php else: ?>
                            <img class="activator" src="<?= $ASSETS ?>/img/product/no-img.png">
                        <?php endif; ?>
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4"><?= $producto->nombre ?><i class="material-icons right">more_vert</i></span>
                        <span class="price">$<?= $producto->precio_vta ?></span>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4"><?= $producto->nombre ?><i class="material-icons right">close</i></span>
                        <p><?= $producto->descripcion ?></p>
                    </div>
                    <div class="card-action center-align yellow accent-4">
                        <a href="#" class="black-text add-cart-btn" idProduct="<?= $producto->id_producto ?>" price="<?= $producto->precio_vta ?>" product-name="<?= $producto->nombre ?>">Add to Cart</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="col s12 center-align">
            <h4>No products found</h4>
        </div>           
    <?php endif; ?>
</div>