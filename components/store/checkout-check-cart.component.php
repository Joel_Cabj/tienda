<div class="row">
    <center><h4><b>CHECK CART</b></h4></center>
    <br>

    <div class="check-cart-section">
    </div>
    <br>
    <div class="col s6 offset-s3 black white-text sub_total_price_col">
        <div class="col s6 left-align">
            <h5><b>Sub Total</b></h5>
        </div>
        <div class="col s6 right-align">
            <h5><b class="sub_total_price">$</b></h5>
            <span class="helper-text white-text">Quantity: <b class="items_quantity">#</b></span><br>
        </div>
    </div>
          
    <div class="col s6 offset-s3 center-align">
        <br>
        <a href="#" class="btn waves-effect waves-light yellow accent-4 black-text btn_next"><i class="material-icons right">navigate_next</i>NEXT</a>           
    </div>               
</div>