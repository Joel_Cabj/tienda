<div class="fixed-action-btn">
  <a class="btn-floating btn-large yellow accent-4">
    <i class="large material-icons tooltipped black-text" data-position="left" data-tooltip="Add">add</i>
  </a>
  <ul>
    <li><a id="addCategory" class="btn-floating white tooltipped" data-position="left" data-tooltip="Category" ><i class="material-icons black-text">device_hub</i></a></li>
    <li><a id="addProduct" class="btn-floating white tooltipped" data-position="left" data-tooltip="Product" ><i class="material-icons black-text">devices_other</i></a></li>
  </ul>
</div>

<?php include($COMPONENTS."admin/add-product-modal.component.php") ?>
<?php include($COMPONENTS."admin/add-category-modal.component.php") ?>

