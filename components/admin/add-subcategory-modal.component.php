<div id="addSubCategoryModal" class="modal">
    <div class="modal-content" id="contentSubCategoryModal">
            <center><h4><b>NEW SUBCATEGORY</b></h4></center>
            <form method="POST">
                <input type="hidden" name="id_categoria" value="<?= $categoria->id_categoria ?>">
                <input type="hidden" name="form" value="5">
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="RGB" id="subcategory_name" name="subcategory_name" type="text" class="validate" required>
                        <label for="subcategory_name">Subcategory Name</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12 center-align">    
                        <button class="btn waves-effect waves-light yellow accent-4 black-text" type="submit">CREATE</button>                       
                    </div>
                </div>
            </form>
        
    </div>
</div>