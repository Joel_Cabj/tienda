<div id="product-overview" class="modal">
  <div class="modal-content">
    <div class="row">
      <div class="col s6 center-align">
        <img class="product-img" id="pov-img" src="<?= $ASSETS ?>img/product/no-img.png">
      </div>
      <div class="col s3">
        <p class="grey-text">Product Name</p>
        <h6><b id="pov-name"></b></h6>
      </div>
      <div class="col s3">
        <p class="grey-text">Product Brand</p>
        <h6><b id="pov-brand"></b></h6>
      </div>
      <div class="col s3">
        <p class="grey-text">Category</p>
        <h6><b id="pov-category"></b></h6>
      </div>
      <div class="col s3">
        <p class="grey-text">Subcategory</p>
        <h6><b id="pov-subCategory"></b></h6>
      </div>
      <div class="col s6">
        <p class="grey-text">Description</p>
        <h6><b id="pov-description"></b></h6>
      </div>
      <div class="col s3">
        <p class="grey-text">Price</p>
        <h6><b id="pov-price"></b></h6>
      </div>
      <div class="col s3">
        <p class="grey-text">Stock</p>
        <h6><b id="pov-stock"></b></h6>
      </div>
      <div class="modal-footer">
        <div class="col s12 center-align">
          <br>
          <a id="pov-url-edit" href="#edit" class="waves-effect waves-green btn yellow accent-4 black-text"><i class="material-icons black-text left">edit</i>EDIT</a>
        </div>
      </div>
    </div>
  </div>
</div>