<div id="editSubcategoryModal" class="modal">
    <div class="modal-content">
        <center><h4><b>EDIT SUBCATEGORY</b></h4></center>
        <form method="POST">
            <input type="hidden" name="id_categoria" value="<?= $categoria->id_categoria ?>">
            <input type="hidden" id="id_subcategoria_edit" name="id_subcategoria" value="#">
            <input type="hidden" name="form" value="6">
            <div class="row">
                <div class="input-field col s12">
                    <input class="validate" type="text" id="subcategory_name_edit" name="subcategory_name_edit" value="#" required>
                    <label for="subcategory_name_edit">Subcategory Name</label>
                </div>
            </div>

            <div class="row">
                <div class="col s6 right-align">    
                    <button class="btn waves-effect waves-light yellow accent-4 black-text" type="submit"><i class="material-icons left">edit</i>EDIT</button>                       
                </div>
                <div class="col s6 left-align">    
                    <a class="btn waves-effect waves-light white black-text modal-close" type="button"><i class="material-icons left">cancel</i>CANCEL</a>                       
                </div>
            </div>
        </form>  
    </div>
</div>