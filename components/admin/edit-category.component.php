<div class="row">
    <div class="col s12 tabs-col">
        <ul class="tabs">
            <li class="tab col s6"><a class="active black-text" href="#category">CATEGORY</a></li>
            <li class="tab col s6"><a class="black-text" href="#subcategory">SUBCATEGORY</a></li>
        </ul>
    </div>
    <div id="category" class="col s12">
        <div class="row">
            <center><h4><b>EDIT CATEGORY</b></h4></center>
            <br>
            <form method="POST">
                <input type="hidden" name="form" value="4">
                <input type="hidden" name="id_categoria" value="<?= $categoria->id_categoria ?>">
                <div class="col s6 offset-s3">
                    <div class="input-field">
                        <input value="<?= $categoria->categoria ?>" name="categoria" id="categoria" type="text" class="validate">
                        <label for="categoria">Category Name</label>
                    </div>
                </div>       
                <div class="col s3 offset-s3 right-align">
                    <br>
                    <button class="btn waves-effect waves-light yellow accent-4 black-text" type="submit" name="action"><i class="material-icons left">save</i>SAVE</button>           
                </div>
                <div class="col s3 left-align">
                    <br>
                    <a href="/tienda/admin/store.php#categories" class="waves-effect waves-green btn red"><i class="material-icons left">cancel</i>CANCEL</a>
                </div>
            </form>                  
        </div>
    </div>
    <div id="subcategory" class="col s12">
        <center><h4><b>EDIT SUBCATEGORY</b></h4></center>   
        <br>

        <div class="col s6 offset-s3">
            <table class="highlight">
                <thead>
                <tr>
                    <th class="center-align">Subcategory Name</th>
                    <th class="center-align">Assigned Products</th>
                    <th class="center-align">Actions</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach($subcategorias as $sub) : ?>
                    <tr class="hover">
                        <td class="center-align btn_edit_subcategory" subcategory_name="<?= $sub->subcategoria ?>" id_subcategory="<?= $sub->id_subcategoria ?>">
                            <?= $sub->subcategoria ?>
                        </td>
                        <td class="center-align btn_edit_subcategory" subcategory_name="<?= $sub->subcategoria ?>" id_subcategory="<?= $sub->id_subcategoria ?>">
                        <?php
                            $subcategory = new Subcategoria();
                            $subcategory->id_subcategoria = $sub->id_subcategoria;
                            $assignedProducts = count($subcategory->getAssignedProducts());
                            echo($assignedProducts);
                            ?>
                        </td>
                        <td class="center-align" subcategory_name="<?= $sub->subcategoria ?>" id_subcategory="<?= $sub->id_subcategoria ?>">
                            <?php
                            // echo('<a class="btn-flat btn_edit_subcategory waves-effect waves-yellow black-text" subcategory_name="'.$sub->subcategoria.'" id_subcategory="'.$sub->id_subcategoria.'"><i class="material-icons left">edit</i>EDIT</a>');

                            if ($assignedProducts <= 0) {
                                echo('<a class="btn-flat btn_remove_subcategory waves-effect waves-red black-text" subcategory_name="'.$sub->subcategoria.'" id_subcategory="'.$sub->id_subcategoria.'"><i class="material-icons left">delete</i>REMOVE</a>');
                            } else {
                                echo "---";
                            }
                            ?> 
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <br>
        </div>

        <div class="col s6 offset-s3 center-align">
            <button class="btn waves-effect waves-light yellow accent-4 black-text add-new-subc" type="button" id-categoria="<?= $categoria->id_categoria ?>"><i class="material-icons left">add</i>ADD SUBCATEGORY</button> 
        </div>
    </div>
</div>