<div id="category-overview" class="modal">
    <div class="modal-content">
        <center><h4 id="cov-name">{category-name}</h4></center>
        <div class="col s12">
            <table class=" highlight centered responsive-table">
                <thead>
                    <tr>
                    <th>Subcategory</th>
                    <th>Assigned Products</th>
                    <th>Hot Products</th>
                    </tr>
                </thead>

                <tbody id="cov-subcategories">
                    <tr>
                        <td>Sin Subcategorias</td>
                        <td></td>
                        <td><i class="material-icons">star</i></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <div class="modal-footer">
                <div class="col s12 center-align">
                    <a id="cov-url-edit" href="#edit" class="waves-effect waves-green btn yellow accent-4 black-text"><i class="material-icons black-text left">edit</i>EDIT</a>
                </div>
            </div>         
        </div>
    </div>
</div>