<div id="addProductModal" class="modal">
    <div class="modal-content">
        <center><h4><b>NEW PRODUCT</b></h4></center>
        <div class="row">
            <form method="POST" enctype="multipart/form-data">
                <input type="hidden" name="form" value="2">
                <div class="input-field col s6">
                    <input placeholder="Nebula X" id="nombre" name="nombre" type="text" class="validate" required>
                    <label for="nombre">Product Name</label>
                </div>
                <div class="input-field col s6">
                    <input placeholder="Corsair" id="marca" name="marca" type="text" class="validate" required>
                    <label for="marca">Product Brand</label>
                </div>
                <div class="input-field col s6">
                    <select required id="productCategorySelect" name="id_categoria" required>
                        <option disabled selected>Assign to a category</option>
                        <?php foreach ($categorias as $c): ?>
                            <option value="<?= $c->id_categoria ?>"><?= $c->categoria ?></option>
                        <?php endforeach; ?>
                    </select>
                    <label>Select Category</label>
                </div>
                <div class="input-field col s6">
                    <select required id="productSubCategorySelect" name="id_subcategoria" required>
                        <option value="" disabled selected>Assign to a subcategory</option>
                        <option value="1">RGB</option>
                        <option value="2">White Series</option>
                        <option value="3">Template Glass</option>
                    </select>
                    <label>Select Subcategory</label>
                </div>
                <div class="input-field col s6">
                    <input placeholder="$1500" id="precio_vta" name="precio_vta" type="number" class="validate" required>
                    <label for="precio_vta">Product Price</label>
                </div>
                <div class="input-field col s6">
                    <input placeholder="10" id="stock" name="stock" type="number" class="validate" required>
                    <label for="stock">Product Stock</label>
                </div>
                    <div class="input-field col s12">
                    <textarea id="descripcion" name="descripcion" class="materialize-textarea"></textarea>
                    <label for="descripcion">Product Description</label>
                </div>
                <div class="file-field input-field col s12">
                    <div class="btn waves-effect waves-light white black-text">
                        <span>Image</span>
                        <input type="file" name="image" id="image" accept="image/*">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                </div>
                <div class="col s6 right-align">
                    <button class="btn waves-effect waves-light yellow accent-4 black-text" type="submit" name="action"><i class="material-icons left">add</i>ADD</button>           
                </div>
                <div class="col s6 left-align">
                    <a href="#!" class="modal-close waves-effect waves-green btn red"><i class="material-icons left">cancel</i>CANCEL</a>
                </div>        
            </form>
        </div>      
    </div>
</div>