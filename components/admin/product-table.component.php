<div class="col s12">
  <table class=" highlight centered responsive-table">
    <thead>
      <tr>
      <th>Name</th>
      <th>Brand</th>
      <th>Category</th>
      <th>Subcategory</th>
      <th>Price</th>
      <th>Hot Product</th>
      </tr>
    </thead>

    <tbody>
      <?php
      foreach ($productos as $p):
      ?>
        <tr>
          <td class="hover product-view" data-id="<?= $p->id_producto ?>"><?= $p->nombre ?></td>
          <td class="hover product-view" data-id="<?= $p->id_producto ?>"><?= $p->marca ?></td>
          <td class="hover product-view" data-id="<?= $p->id_producto ?>"><?= $p->getCategoria() ?></td>
          <td class="hover product-view" data-id="<?= $p->id_producto ?>"><?= $p->getSubCategoria() ?></td>
          <td class="hover product-view" data-id="<?= $p->id_producto ?>">$<?= $p->precio_vta ?></td>
        <?php 
          if ($p->destacado === 'true') {
            echo '<td><i class="material-icons hover hot-product-btn" data-id="'.$p->id_producto.'">star</i></td>';
          } else {
            echo '<td><i class="material-icons hover hot-product-btn" data-id="'.$p->id_producto.'">star_border</i></td>';
          }
        ?>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div> 