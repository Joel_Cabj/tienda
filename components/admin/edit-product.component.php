<center><h4><b>EDIT PRODUCT</b></h4></center>
<br>
<div class="row">
    <form method="POST" enctype="multipart/form-data" action="/tienda/admin/store.php">
        <input type="hidden" name="form" value="3">
        <input type="hidden" name="id_producto" value="<?= $producto->id_producto ?>">
        <div class="input-field col s3 offset-s3">
            <input id="nombre" name="nombre" type="text" class="validate" value="<?= $producto->nombre ?>" required>
            <label for="nombre">Product Name</label>
        </div>
        <div class="input-field col s3">
            <input id="marca" name="marca" type="text" class="validate" value="<?= $producto->marca ?>" required>
            <label for="marca">Product Brand</label>
        </div>
        <div class="input-field col s6 offset-s3">
            <select required name="estado">
                <option value="1" <?php ($producto->estado === 1) ? 'selected' : ''; ?>>Habilitado</option>
                <option value="0" <?php ($producto->estado === 0) ? 'selected' : ''; ?>>Deshabilitado</option>
            </select>
            <label>Product State</label>
        </div>
        <div class="input-field col s3 offset-s3">
            <select name="id_categoria" id="productCategorySelect">
                <?php foreach($categorias as $c) : ?>
                    <option value="<?= $c->id_categoria ?>" <?= ($producto->id_categoria === $c->id_categoria) ? 'selected' : ''; ?>><?= $c->categoria ?></option>
                <?php endforeach; ?>
            </select>
            <label>Assigned Category</label>
        </div>
        <div class="input-field col s3">
            <select name="id_subcategoria" id="productSubCategorySelect">
                <option value="0" disabled>Assign to a subcategory</option>
                <?php foreach($subcategorias as $s) : 
                        if ($s->id_categoria === $producto->id_categoria) :
                ?>
                            <option value="<?= $s->id_subcategoria ?>" <?= ($producto->id_subcategoria === $s->id_subcategoria) ? 'selected' : ''; ?>><?= $s->subcategoria ?></option>
                <?php 
                        endif;
                    endforeach; ?>
            </select>
            <label>Assigned Subcategory</label>
        </div>
        <div class="input-field col s3 offset-s3">
            <input id="product_price" name="precio_vta" type="number" class="validate" value="<?= $producto->precio_vta ?>">
            <label for="product_price">Product Price</label>
        </div>
        <div class="input-field col s3">
            <input id="product_stock" name="stock" type="number" class="validate" value="<?= $producto->stock ?>">
            <label for="product_stock">Product Stock</label>
        </div>
            <div class="input-field col s6 offset-s3">
            <textarea id="product_description" name="descripcion" class="materialize-textarea"><?= $producto->descripcion ?></textarea>
            <label for="product_description">Product Description</label>
        </div>
        <div class="file-field input-field col s6 offset-s3">
            <div class="btn waves-effect waves-light white black-text">
                <span>Image</span>
                <input type="file" name="image" id="image" accept="image/jpg, image/png, image/jpeg">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
            </div>
        </div>
        <div class="col s2 offset-s3 right-align">
            <button class="btn waves-effect waves-light yellow accent-4 black-text" type="submit" name="action"><i class="material-icons left">save</i>SAVE</button>           
        </div>
        <div class="col s2 left-align">
            <a href="/tienda/admin/store.php#products" class="waves-effect waves-green btn red"><i class="material-icons left">cancel</i>CANCEL</a>
        </div>
    </form>
</div>      