<div id="removeSubcategoryModal" class="modal">
    <div class="modal-content">
        <center><h4><b>REMOVE SUBCATEGORY</b></h4></center>
        <form method="POST">
            <input type="hidden" name="id_categoria" value="<?= $categoria->id_categoria ?>">
            <input type="hidden" id="id_subcategoria_remove" name="id_subcategoria" value="#">
            <input type="hidden" name="form" value="7">
            <div class="row">
                <div class="input-field col s12 center-align">
                    <h5>Are you sure you want to remove <b id="subcategory_name">subcategory_name</b>?</h5>
                </div>
                <div class="col s6 input-field right-align">
                <p>
                    <label>
                        <input id="yes_radio" class="with-gap" type="radio" name="confirm_delete"/>
                        <span>YES</span>
                    </label>
                </p>
                </div>
                <div class="col s6 input-field left-align">
                <p>
                    <label>
                        <input id="no_radio" class="with-gap" type="radio" name="confirm_delete" checked/>
                        <span>NO</span>
                    </label>
                </p>
                </div>
            </div>

            <div class="row">
                <div class="col s6 right-align">    
                    <button class="btn btn_remove_subcategory_submit waves-effect waves-light red white-text" type="submit" disabled><i class="material-icons left">delete</i>REMOVE</button>                       
                </div>
                <div class="col s6 left-align">    
                    <a class="btn waves-effect waves-light white black-text modal-close" type="submit"><i class="material-icons left">cancel</i>CANCEL</a>                       
                </div>
            </div>
        </form>  
    </div>
</div>