<div id="addCategoryModal" class="modal">
    <div class="modal-content" id="contentModalCategory">
        <div class="row">
            <center><h4><b>NEW CATEGORY</b></h4></center>
            <br>
            <form method="POST">
                <input type="hidden" name="form" value="1"> <!-- 1: Crear Categoría-->
                <div class="input-field col s6">
                    <input placeholder="Keyboards" id="category_name" name="category_name" type="text" class="validate" required>
                    <label for="category_name">Category Name</label>
                </div>
                <div class="input-field col s6">
                    <input placeholder="RGB" id="subcategory_name" name="subcategory_name" type="text" class="validate" required>
                    <label for="subcategory_name">Subcategory Name</label>
                </div>
                <div class="col s6 right-align">
                    <button class="btn waves-effect waves-light yellow accent-4 black-text" type="submit"><i class="material-icons left">add</i>ADD</button>           
                </div>
                <div class="col s6 left-align">
                    <a href="#!" class="modal-close waves-effect waves-green btn red"><i class="material-icons left">cancel</i>CANCEL</a>
                </div>                   
            </form>
        </div>
    </div>
</div>