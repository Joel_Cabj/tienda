<div class="col s12">
    <table class=" highlight centered responsive-table">
        <thead>
            <tr>
            <th>Name</th>
            <!-- <th>Subcategory(ies)</th> -->
            <th>Assigned Products</th>
            <th>Hot Products</th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($categorias as $c):
            ?>
            <tr class="hover">
                <td class="category-view" data-id="<?= $c->id_categoria ?>"><?= $c->categoria ?></td>
                <!-- <td>RGB, White Series, Carbon Series</td> -->
                <td class="category-view" data-id="<?= $c->id_categoria ?>"><?= count($c->getAllProductos()) ?></td>
                <td class="category-view" data-id="<?= $c->id_categoria ?>">
                    <?php 
                        $prods = $c->gethotProducts();
                        if (count($prods) > 0) :
                    ?>
                            <i class="material-icons">star</i>
                    <?php else: ?>
                            <i class="material-icons">star_border</i>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>