<ul id="sidenav-user" class="sidenav">
	<?php if ($Login->activa()) : ?>
		<li>
		<div class="user-view">
			<a href="#name"><span class="black-text name"><?= $Login->getNombreCompleto() ?></span></a>
			<a href="#email"><span class="black-text email"><?= $Login->getEmail(); ?></span></a>
		</div>
		</li>
		<li class="btn-sign-off sidenav-close center-align"><a href="/tienda/src/login/?cerrar=close"><i class="material-icons right">person_outlined</i>SIGN OFF</a></li>
	<?php else: ?>
		<li class="btn-login sidenav-close center-align"><a href="#"><i class="material-icons right">person</i>LOGIN / REGISTER</a></li>
	<?php endif; ?>    
	<li><div class="divider sidenav-divider"></div></li>
  	<li class="center-align"><a class="subheader">Your cart is empty</a></li>
  	<li class="center-align"><a href="/tienda/store"><i class="material-icons right">shopping_cart</i>Let's go shopping</a></li>
</ul>