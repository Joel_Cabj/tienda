<div class="container">
    <form method="post" action="/tienda/src/login/">
        <h4 class="center-align">Hi! Please login to continue the operation</h4>
        <div class="row">
            <div class="input-field col s10 offset-s1">
                <input type="email" id="email" name="email" class="validate" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
                <label for="email">Email</label>
            </div>
            <div class="input-field col s10 offset-s1">
                <input type="password" id="clave" name="clave" class="validate" required>
                <label for="clave">Password</label>
            </div>
            <div class="col s6 right-align">
                <a class="btn btn_register waves-effect waves-yellow white black-text">REGISTER</a>
            </div>
            <div class="col s6 left-align">
                <button class="btn waves-effect waves-light yellow accent-4 black-text" type="submit" name="action">CONNECT</button>
            </div>
        </div>     
    </form>
</div>