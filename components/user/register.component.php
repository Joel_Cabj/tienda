<div class="container">
    <form method="post" action="/tienda/src/register/index.php">
        <h4 class="center-align">Welcome! Please register to continue the operation</h4>
        <div class="row">
            <div class="input-field col s6">
                <input id="nombre" name="nombre" type="text" class="validate" pattern="[a-zA-Z]+" required>
                <label for="nombre">Name</label>
            </div>            
            <div class="input-field col s6">
                <input id="apellido" name="apellido" type="text" class="validate"  pattern="[a-zA-Z]+" required>
                <label for="apellido">Surname</label>
            </div>
            <div class="input-field col s12">
                <input type="email" id="email" name="email" class="validate" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
                <label for="email">Email</label>
            </div>
            <div class="input-field col s12">
                <input type="password" id="clave" name="clave" class="validate" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" required>
                <label for="clave">Password</label>
            </div>
            <div class="col s6 right-align">
                <a class="btn btn_login waves-effect waves-yellow white black-text">LOGIN</a>
            </div>
            <div class="col s6 left-align">
                <button class="btn waves-effect waves-light yellow accent-4 black-text" type="submit" name="action">SUBMIT</button>
            </div>
        </div>                  
    </form>
</div>