<div class="category-banner">
    <?php if ($categoria->categoria != ''): ?>
        <h2 class="category-banner-title"><?= $categoria->categoria ?></h2>
    <?php else: ?>
        <h2 class="category-banner-title">CATEGORY</h2>
    <?php endif; ?>
</div>