<header>
    <div class="navbar-fixed">
        <nav>
        <div class="nav-wrapper">
            <div class="container header">
                <div class="row">
                    <div class="col s12">
                        <a href="#" data-target="sidenav-menu" class="left sidenav-trigger hide-on-medium-and-up"><i class="material-icons nav-icon">menu</i></a>
                        <?php if ($Login->getRol() === 'admin'): ?>
                            <a href="/tienda/admin/" class="brand-logo hide-on-small-only"><img class="svg-logo" src="<?= $ASSETS ?>img/logo_white.svg" alt="Corsair"></a>
                            <a href="/tienda/admin/" class="brand-logo hide-on-med-and-up"><img class="svg-logo-res" src="<?= $ASSETS ?>img/logo_mobile_white.svg" alt="Cosair"></a>
                            <a href="#" data-target="sidenav-user" class="right sidenav-trigger show-on-medium-and-up"><i class="material-icons nav-icon">person</i></a>
                        <?php else: ?>
                            <a href="/tienda" class="brand-logo hide-on-small-only"><img class="svg-logo" src="<?= $ASSETS ?>img/logo_black.svg" alt="Corsair"></a>
                            <a href="/tienda" class="brand-logo hide-on-med-and-up"><img class="svg-logo-res" src="<?= $ASSETS ?>img/logo_mobile_black.svg" alt="Cosair"></a>
                            <?php if ($Login->activa()) : ?>
                                <a href="#" data-target="sidenav-user" class="user-sidenav right sidenav-trigger show-on-medium-and-up"><i class="material-icons nav-icon">person</i></a>
                            <?php else: ?>
                                <a href="#" data-target="sidenav-user" class="user-sidenav right sidenav-trigger show-on-medium-and-up"><i class="material-icons nav-icon">person_outlined</i></a>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php require_once($COMPONENTS."/common/navmenu.component.php") ?>
                    </div>
                </div>
            </div>			
        </div> 
        </nav>
    </div>
    <?php require_once($COMPONENTS."common/progress-bar.component.php") ?>
</header>