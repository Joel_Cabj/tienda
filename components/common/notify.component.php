<?php if (isset($_GET['codeError'])): ?>

    <?php if ($_GET['codeError'] === '1'): ?>
        <script>
            var ask = confirm('Contraseña incorrecta');
        </script>
    
    <?php elseif ($_GET['codeError'] === '2'): ?>
        <script>
            var ask = confirm('Usuario no registrado');
        </script>
    
    <?php elseif ($_GET['codeError'] === '3'): ?>
        <script>
            var ask = confirm('Sesión Activa');
        </script>
    
    <?php else: ?>
        <script>
            var ask = confirm('Error desconocido');
        </script>
    
    <?php endif; ?>

<?php endif; ?>