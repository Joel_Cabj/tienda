<ul class="right hide-on-med-and-down">
    <?php if ($Login->getRol() === 'admin'): ?>
        <li><a class="menu-item" href="/tienda/admin/dashboard.php">DASHBOARD</a></li>
        <li><a class="menu-item" href="/tienda/admin/store.php">STORE</a></li>
        <li><a class="menu-item" href="#">USERS</a></li>
    <?php else: ?>
        <li><a class="menu-item" href="/tienda/store/">STORE</a></li>
        <li><a class="menu-item" href="#">COMMUNITY</a></li>
        <li><a class="menu-item" href="#">SUPPORT</a></li>
    <?php endif; ?>
</ul>