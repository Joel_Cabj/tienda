<footer class="page-footer">
  <li class="divider yellow accent-4"></li>
  <div class="footer-copyright">
    <div class="container white-text">Copyright © <?php echo date('Y') ?> <b>CORSAIR</b>.
    <a class="modal-trigger white-text right" href="#modal">Privacy Policy</a>
  </div>    

  <div id="modal" class="modal">
      <div class="modal-content grey-text text-darken-2">
        <h5>Terms & Conditions</h5>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur temporibus consectetur quaerat obcaecati dolor veniam suscipit aperiam eius perspiciatis tempora maiores officiis commodi dolorem facere, error ut natus architecto optio.</p>
      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
      </div>
  </div>
</footer>