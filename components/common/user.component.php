<ul id="sidenav-user" class="sidenav">
	<?php if ($Login->getRol() === 'admin'): ?>
		<li>
			<div class="user-view center-align">
				<a href="/tienda/user/"><span class="black-text name"><?= $Login->getNombreCompleto() ?></span></a>
				<a href="/tienda/user/"><span class="black-text email"><?= $Login->getEmail(); ?></span></a>
			</div>
		</li>
		<li class="btn-sign-off sidenav-close center-align"><a href="/tienda/src/login/?cerrar=close"><i class="material-icons right">person_outlined</i>SIGN OFF</a></li>	
		<div class="divider"></div>    
		<li class="center-align"><a class="subheader">Workstation</a></li>
		<li class="center-align"><a href="mailto:support@localhost.com">Contact</a></li>
		<li class="center-align"><a href="/tienda/docs/">Docs</a></li>
	<?php else: ?>
		<?php if ($Login->activa()) : ?>
			<li class="btn-sign-off sidenav-close center-align"><a href="/tienda/user/"><i class="material-icons right">face</i>VIEW PROFILE</a></li>
			<li>
				<div class="user-view">
					<a href="/tienda/user/"><span class="black-text name"><?= $Login->getNombreCompleto() ?></span></a>
					<a href="/tienda/user/"><span class="black-text email"><?= $Login->getEmail(); ?></span></a>
				</div>
			</li>
			<li class="btn-sign-off sidenav-close center-align"><a href="/tienda/src/login/?cerrar=close"><i class="material-icons right">person_outlined</i>SIGN OFF</a></li>
		<?php else: ?>
			<li class="btn-login sidenav-close center-align"><a href="#"><i class="material-icons right">person</i>LOGIN / REGISTER</a></li>
		<?php endif; ?>    
			<li><div class="divider sidenav-divider"></div></li>
			<div class="cart-section"></div>
	<?php endif; ?>
</ul>