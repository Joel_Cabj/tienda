<div id="modalLogin" class="modal white">
    <div class="modal-content login-register-content">
        <div class="row tabs-rows">
            <ul class="tabs grey lighten-4">
                <li class="tab col s6"><a class="active black-text" href="#login">LOGIN</a></li>
                <li class="tab col s6"><a class="black-text" href="#register">REGISTER</a></li>
            </ul>
        </div>

        <div id="login" class="col s12">
            <div class="container modal-containers">
                <form method="post" action="/tienda/src/login/">
                    <h4 class="modal-titles">Hello</h4>
                    <div class="row modal-rows">
                        <div class="input-field col s12">
                            <input type="email" id="email" name="email" class="validate" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
                            <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row modal-rows">
                        <div class="input-field col s12">
                            <input type="password" id="clave" name="clave" class="validate" required>
                            <label for="clave">Contraseña</label>
                        </div>
                    </div>
                    <center>
                        <button class="btn waves-effect waves-light yellow accent-4 black-text" type="submit" name="action">CONNECT</button>
                    </center>       
                </form>
            </div>                
        </div>

        <div id="register" class="col s12">
            <div class="container modal-containers">
                <form method="post" action="/tienda/src/register/index.php">
                    <h4 class="modal-titles">Welcome</h4>
                    <div class="row modal-rows">
                        <div class="input-field col s6">
                            <input id="nombre" name="nombre" type="text" class="validate" pattern="[a-zA-Z]+" required>
                            <label for="nombre">Nombre</label>
                        </div>            
                        <div class="input-field col s6">
                            <input id="apellido" name="apellido" type="text" class="validate"  pattern="[a-zA-Z]+" required>
                            <label for="apellido">Apellido</label>
                        </div>
                    </div>
                    <div class="row modal-rows">
                        <div class="input-field col s12">
                            <input type="email" id="email" name="email" class="validate" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
                            <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row modal-rows">
                        <div class="input-field col s12">
                            <input type="password" id="clave" name="clave" class="validate" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" required>
                            <label for="clave">Contraseña</label>
                        </div>
                    </div>
                    <center>
                        <button class="btn waves-effect waves-light yellow accent-4 black-text" type="submit" name="action">SUBMIT</button>
                    </center>                   
                </form>
            </div>           
        </div>       
    </div>
</div>