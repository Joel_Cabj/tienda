<?php if ( count($categorias) > 0) : ?> 
    <center><h5><b>CATEGORIES</b></h5></center>
<?php elseif ( count($subCategorias) > 0): ?>
    <center><h5><b>SUBCATEGORIES</b></h5></center>
<?php endif; ?>

<div class="collection category-explorer">
    <?php if ( count($categorias) > 0) : ?> 
        <?php foreach($categorias as $categoria) : ?>
            <a href="/tienda/store/category.php?idCategoria=<?= $categoria->id_categoria ?>" class="collection-item black-text"><?= strtoupper($categoria->categoria) ?></a>
        <?php endforeach ?>
    <?php elseif ( count($subCategorias) > 0): ?>
        <?php foreach($subCategorias as $subcategoria) : ?>
            <a href="/tienda/store/category.php?idCategoria=<?= $subcategoria->id_categoria ?>&idSubcategoria<?= $subcategoria->id_subcategoria ?>" class="collection-item black-text"><?= strtoupper($subcategoria->subcategoria) ?></a>
        <?php endforeach ?>
    <?php else: ?>
        <a href="/tienda/store/#categoria" class="collection-item black-text">Sin categorías</a>
    <?php endif; ?>
</div>