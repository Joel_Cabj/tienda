<?php $topics = array('welcome', 'intro', 'test'); ?>
<center><h5><b>TOPICS</b></h5></center>
<div class="collection">
    <?php foreach($topics as $topic) : ?>
        <a id="<?= $topic ?>" href="#<?= $topic ?>" class="collection-item"><?= strtoupper($topic) ?></a>
    <?php endforeach; ?>
</div>
            