<?php
 $CONFIGS = include("./config.php");
 list($SRC, $COMPONENTS, $ASSETS) = $CONFIGS;
 require_once($SRC."/_bd/bd.php");
 require_once($SRC."login/Login.php");
 $Login = new Login();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>CORSAIR</title>
    <link rel="icon" href="<?= $ASSETS ?>img/logo_mobile_black.svg">
    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/common.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/home.css"  media="screen,projection"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
    <?php require_once($COMPONENTS."common/header.component.php") ?>
    <?php require_once($COMPONENTS."common/sidemenu.component.php") ?>
    <?php require_once($COMPONENTS."common/user.component.php") ?>
    <?php require_once("home.php") ?>
    <?php require_once($COMPONENTS."common/login-register.component.php") ?>
    <?php require_once($COMPONENTS."common/footer.component.php") ?>
       
    <script type="text/javascript" src="<?= $ASSETS ?>js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="<?= $ASSETS ?>js/materialize.min.js"></script>
    <script type="text/javascript" src="<?= $ASSETS ?>js/common.js"></script>
    <script type="text/javascript" src="<?= $ASSETS ?>js/home.js"></script>    
</body>
</html>