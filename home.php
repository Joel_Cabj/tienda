<main>
  <div class="slider">
      <ul class="slides">
        <li>
          <img src="<?= $ASSETS ?>img/banner_rgb_case.jpg">
          <div class="caption left-align yellow-text text-accent-4">
          <h1><b>RGB</b> SERIES</h1>
            <h5 class="light white-text">PC CASES.</h5>
          </div>
        </li>
        <li>
          <img src="<?= $ASSETS ?>img/banner_keyboard.jpg">
          <div class="caption left-align yellow-text text-accent-4">
            <h1>MAKE IT <b>YOURS</b></h1>
            <h5 class="light grey-text text-lighten-3">FULL CUSTOMIZABLE.</h5>
          </div>
        </li>
        <li>
          <img src="<?= $ASSETS ?>img/banner_hero.jpg">
          <div class="caption left-align yellow-text text-accent-4">
            <h1><b>HARD</b> GAMING</h1>
            <h5 class="light white-text">TEAM CORSAIR.</h5>
          </div>
        </li>
        <li>
          <img src="<?= $ASSETS ?>img/banner_ambient_lighting.jpg">
          <div class="caption left-align yellow-text text-accent-4">
            <h1>AMBIENT <b>LIGHTING</b></h1>
            <h5 class="light grey-text text-lighten-3">SYMPHONY OF COLOR.</h5>
          </div>
        </li>
      </ul>
  </div>

  <div class="section white">
      <div class="carousel explore-products">
          <h4 class="center-align black-text"><b>EXPLORE</b> PRODUCTS</h2>
          <div href="" class="carousel-item explore-products">
              <img class="responsive-img" src="<?= $ASSETS ?>img/carousel_case.png">
              <h6 class="product-name center-align">CASES</h6>
          </div>
          <div href="" class="carousel-item explore-products">
              <img class="responsive-img" src="<?= $ASSETS ?>img/carousel_keyboard.png">
              <h6 class="product-name center-align">KEYBOARDS</h6>
          </div>
          <div href="" class="carousel-item explore-products">
              <img class="responsive-img" src="<?= $ASSETS ?>img/carousel_mice.png">
              <h6 class="product-name center-align">MICE</h6>
          </div>
          <div href="" class="carousel-item explore-products">
              <img class="responsive-img" src="<?= $ASSETS ?>img/carousel_headset.png">
              <h6 class="product-name center-align">HEADSETS</h6>
          </div>
          <div href="" class="carousel-item explore-products">
              <img class="responsive-img" src="<?= $ASSETS ?>img/carousel_cpu_cooler.png">
              <h6 class="product-name center-align">CPU COOLERS</h6>
          </div>
          <div href="" class="carousel-item explore-products">
              <img class="responsive-img" src="<?= $ASSETS ?>img/carousel_fan.png">
              <h6 class="product-name center-align">FANS</h6>
          </div>
          <div href="" class="carousel-item explore-products">
              <img class="responsive-img" src="<?= $ASSETS ?>img/carousel_memory.png">
              <h6 class="product-name center-align">MEMORY</h6>
          </div>
          <div href="" class="carousel-item explore-products">
              <img class="responsive-img" src="<?= $ASSETS ?>img/carousel_power_supply.png">
              <h6 class="product-name center-align">POWER SUPPLIES</h6>
          </div>
      </div>
  </div>
  <div class="section grey darken-3">
      <h4 class="center-align white-text">CORSAIR IN <b>ACTION</b></h2>
  </div>
  <div class="row row-banners">
      <img class="col l4 m4 s12 responsive-img footer-banners" src="<?= $ASSETS ?>img/footer_banner_1.jpg">
      <img class="col l4 m4 s12 responsive-img footer-banners" src="<?= $ASSETS ?>img/footer_banner_2.jpg">
      <img class="col l4 m4 s12 responsive-img footer-banners" src="<?= $ASSETS ?>img/footer_banner_3.jpg">
  </div>
</main>