
CREATE TABLE `Roles` (
  `id_rol` int(3) NOT NULL,
  `rol` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `Usuarios` (
  `id_usuario` int(3) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `apellido` varchar(25) NOT NULL,
  `dni` int(10),
  `email` varchar(50) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `direccion` varchar(50),
  `ciudad` varchar(50),
  `provincia` varchar(50),
  `id_rol` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `Categorias` (
  `id_categoria` int(3) NOT NULL,
  `categoria` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `Subcategorias` (
  `id_subcategoria` int(3) NOT NULL,
  `subcategoria` varchar(25) NOT NULL,
  `id_categoria` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `Productos` (
  `id_producto` int(3) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `marca` varchar(30) NOT NULL,
  `img` varchar(50),
  `precio_vta` decimal(10,2) NOT NULL,
  `stock` int(4) NOT NULL,
  `destacado` enum('true', 'false') NOT NULL,
  `estado` int(1) NOT NULL DEFAULT 1,
  `descripcion` varchar(50) NOT NULL,
  `id_categoria` int(3) NOT NULL,
  `id_subcategoria` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `Ventas` (
  `id_venta` int(3) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `fecha` datetime  NOT NULL,
  `id_usuario` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `Ordenes` (
  `id_producto` int(3) NOT NULL,
  `cantidad` int(3) NOT NULL,
  `precio_vta` decimal(10,2) NOT NULL,
  `id_venta` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


ALTER TABLE `Roles` ADD PRIMARY KEY (`id_rol`);
ALTER TABLE `Roles` MODIFY `id_rol` int(3) NOT NULL AUTO_INCREMENT;

ALTER TABLE `Usuarios` ADD PRIMARY KEY (`id_usuario`);
ALTER TABLE `Usuarios` MODIFY `id_usuario` int(3) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Usuarios` ADD UNIQUE KEY `dni` (`dni`);
ALTER TABLE `Usuarios` ADD UNIQUE KEY `email` (`email`);

ALTER TABLE `Categorias` ADD PRIMARY KEY (`id_categoria`);
ALTER TABLE `Categorias` MODIFY `id_categoria` int(3) NOT NULL AUTO_INCREMENT;

ALTER TABLE `Subcategorias` ADD PRIMARY KEY (`id_subcategoria`);
ALTER TABLE `Subcategorias` MODIFY `id_subcategoria` int(3) NOT NULL AUTO_INCREMENT;

ALTER TABLE `Productos` ADD PRIMARY KEY (`id_producto`);
ALTER TABLE `Productos` MODIFY `id_producto` int(3) NOT NULL AUTO_INCREMENT;

ALTER TABLE `Ventas` ADD PRIMARY KEY (`id_venta`);
ALTER TABLE `Ventas` MODIFY `id_venta` int(3) NOT NULL AUTO_INCREMENT;


ALTER TABLE `Usuarios`
  ADD CONSTRAINT `Usuarios_Roles` FOREIGN KEY (`id_rol`) REFERENCES `Roles` (`id_rol`);

ALTER TABLE `Subcategorias`
  ADD CONSTRAINT `Subcategorias_Categorias` FOREIGN KEY (`id_categoria`) REFERENCES `Categorias` (`id_categoria`);

ALTER TABLE `Productos`
  ADD CONSTRAINT `Productos_Categorias` FOREIGN KEY (`id_categoria`) REFERENCES `Categorias` (`id_categoria`),
  ADD CONSTRAINT `Productos_Subcategorias` FOREIGN KEY (`id_subcategoria`) REFERENCES `Subcategorias` (`id_subcategoria`);

ALTER TABLE `Ventas`
  ADD CONSTRAINT `Ventas_Usuarios` FOREIGN KEY (`id_usuario`) REFERENCES `Usuarios` (`id_usuario`);
  
ALTER TABLE `Ordenes`
  ADD CONSTRAINT `Ventas_Ordenes` FOREIGN KEY (`id_venta`) REFERENCES `Ventas` (`id_venta`),
  ADD CONSTRAINT `Productos_Ordenes` FOREIGN KEY (`id_producto`) REFERENCES `Productos` (`id_producto`);


INSERT INTO `Roles` (`id_rol`, `rol`) VALUES
(1, 'admin'),
(2, 'usuario');

INSERT INTO `Categorias` (`id_categoria`, `categoria`) VALUES
(0, 'Varios');

INSERT INTO `Subcategorias` (`id_subcategoria`, `subcategoria`, `id_categoria`) VALUES
(0, 'Varios', 0);