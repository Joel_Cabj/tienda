<?php
require_once '../_bd/bd.php';
require_once '../clases/Usuario.php';
require_once './Login.php';
$bd = new bd();

if (!empty($_POST)) {

    if (isset($_POST['email']) && isset($_POST['clave'])){
        $email = $_POST['email'];
        $clave = $_POST['clave'];
        $Login = new Login($email, $clave);

        if (!isset($_SESSION['activa'])) {
            if($Login->iniciar()) {
                setcookie("id_usuario", $_SESSION["id_usuario"]);
                setcookie("email", $_SESSION["email"]);
                setcookie("nombre", $_SESSION["nombre"]);
                setcookie("apellido", $_SESSION["apellido"]);
                setcookie("rol", $_SESSION["rol"]);
                
                if ($_SESSION["rol"] === 'admin') {
                    header('Location: ../../admin');
                } else {
                    header('Location: ../../store');
                }
    
            } else {
                switch($Login->getError()) {
                    case 'WrongPassword':
                        header('Location: ../../?codeError=1'); // Contraseña erronea.
                        break;
                    case 'InvalidEmail':
                        header('Location: ../../?codeError=2'); // El usuario no existe.
                        break;
                    default:
                        header('Location: ../../?codeError=0'); // Error query.
                        break;
                }
            }
        } else {
            header('Location: ../../?codeError=3'); // Sesión activa.
        }

    }

} else {
    if (isset($_GET['cerrar'])) {
        $Login = new Login();
        $Login->cerrar();
        header('Location: /tienda');
    } else {
        header('Location: ../../?codeError=4'); // No post.
    }
}