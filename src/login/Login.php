<?php

// require_once '../_bd/bd.php';
// require_once '../clases/Usuario.php';

class Login {

    private $BASEDATOS;

    private $email;
    private $clave;

    private $ERROR;

    public function __construct($email='', $clave=0) {
        global $bd;
        $this->BASEDATOS = $bd;
        $this->email = $email;
        $this->clave = md5($clave);

        if (!session_start()) {
            $this->ERROR = 'NoSession';
            return false;
        } else {
            return true;
        }
    }
    
    
    public function iniciar() {
        $email = $this->email;
        $clave = $this->clave;
        $sql = "SELECT * FROM usuarios WHERE email = '$email'";
        $response = $this->BASEDATOS->q($sql)->fetch_assoc();
        if ($response) {
            if ( $response['clave'] === $clave) {
                $usuario = new Usuario($response['id_usuario']);
                $usuario->getUsuario();
                // session_start();
                $_SESSION['id_usuario'] = $usuario->id_usuario;
                $_SESSION['email'] = $email;
                $_SESSION['clave'] = $clave;
                $_SESSION['nombre'] = $usuario->nombre;
                $_SESSION['apellido'] = $usuario->apellido;
                $_SESSION['rol'] = $usuario->getRol();
                $_SESSION['clave'] = $clave;
                $_SESSION['activa'] = true;
                return true;
            } else {
                $this->ERROR = "WrongPassword";
                return false;
            }
        } else {
            $this->ERROR = "InvalidEmail";
            return false;
        }
        
    }


    public function validar() {
        $email = '';
        $clave = '';

        if (isset($_SESSION['email'])) {
            $email = $_SESSION['email'];
        } else {
            $this->ERROR = 'NoEmail';
            return false;
        }
        
        if (isset($_SESSION['clave'])) {
            $clave = $_SESSION['clave'];
        } else {
            $this->ERROR = 'NoPassword';
            return false;
        }
        
        try {
            $sql = "SELECT * FROM usuarios WHERE email = '$email' AND clave = '$clave'";
            $response = $this->BASEDATOS->q($sql);
            
            if (!$response) {
                $this->ERROR = 'QueryError';
                return false;
            } else {
               return true;
            }
        } catch (Exception $e) {
            $this->ERROR = 'Error de Base de Datos ' . $e->getMessage();
            return false;
        }
    }

    public function activa() {
        if (isset($_SESSION['activa'])) {
            return $_SESSION['activa'];
        } else {
            $this->ERROR = 'NoSession';
            return false;
        }
    }

    public function cerrar() {
        if (!session_destroy()) {
            $this->ERROR = 'NoCloseSession';
            return false;
        } else {
            setcookie("id_usuario", null);
            setcookie("email", null);
            setcookie("nombre", null);
            setcookie("apellido", null);
            setcookie("rol", null);
            return true;
        }
    }

    public function getError() {
        return $this->ERROR;
    }
	
	public function get_idUsuario(){
		return $_SESSION['id_usuario'];
    }
    
    public function getNombreCompleto() {
        if (isset($_SESSION['nombre']) || isset($_SESSION['apellido'])) {
            return $_SESSION['nombre'] . " " . $_SESSION['apellido'];
        } else {
            return '';
        }
    }

    public function getEmail() {
        if (isset($_SESSION['email'])) {
            return $_SESSION['email'];
        } else {
            return '';
        }
    }


    public function get_usuario() {
        if ($this->activa()) {
            if (isset($_SESSION['usuario'])) {
                return $_SESSION['usuario'];
            } else {
                $this->ERROR = 'No está seteado el nombre de usuario';
                return false;
            }
        } else {
            $this->ERROR = 'No tiene una sección activa';
            return false;
        }
    }


    public function getRol() {
        if ($this->activa()) {
            if (isset($_SESSION['rol'])) {
                return $_SESSION['rol'];
            } else {
                $this->ERROR = 'No está seteado el Rol';
                return '';
            }
        } else {
            $this->ERROR = 'No tiene una sección activa';
            return '';
        }
    }

}

