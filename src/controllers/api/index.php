<?php

$CONFIGS = include("../../../config.php");
list($SRC, $COMPONENTS, $ASSETS) = $CONFIGS;
require_once($SRC."clases/Categoria.php");
require_once($SRC."clases/Subcategoria.php");
require_once($SRC."clases/Producto.php");
require_once($SRC."/_bd/bd.php");
$bd = new bd();
$producto = new Producto();
$categoria = new Categoria();
$subCategoria = new Subcategoria();
$categorias = array();
$subCategorias = array();
$productos = array();

if (!empty($_GET)) {
    if (isset($_GET['idCategoria'])) {
        $categoria->id_categoria = intval($_GET['idCategoria']);
        $categoria->getCategoria();
        $subCategorias = $categoria->getSubCategorias();
        $response = new stdClass();
        $response->categoria = $categoria;
        $subs = array();
        foreach ($subCategorias as $s) {
            $sub = new Subcategoria();
            $sub->id_subcategoria = $s->id_subcategoria;
            $sb = new stdClass();
            $sb->id_subcategoria = $s->id_subcategoria;
            $sb->subcategoria = $s->subcategoria;
            $sb->assignedProducts = count($sub->getAssignedProducts());
            $sb->hotProducts = count($sub->gethotProducts());
            array_push($subs, $sb);
        }
        $response->subCategorias = $subs;
        header('Content-type: application/json');
        echo json_encode($response);
    } elseif (isset($_GET['idProducto'])) {
        $producto->id_producto = intval($_GET['idProducto']);
        $producto->getProducto();
        $producto->categoria = $producto->getCategoria();
        $producto->subcategoria = $producto->getSubCategoria();
        $response = new stdClass();
        $response->producto = $producto;
        header('Content-type: application/json');
        echo json_encode($response);
    }
}

if (!empty($_POST)) {
    if(isset($_POST['hotProduct'])){
        $producto->id_producto = intval($_POST['idProducto']);
        $response = new stdClass();
        $response->ok = false;
        $response->destacado = 'false';
        if ($producto->hotProduct()) {
            $response->ok = true;
            $response->destacado = $producto->destacado;
        }
        header('Content-type: application/json');
        echo json_encode($response);
    }
}