<?php
date_default_timezone_set('America/Argentina/Buenos_Aires');

$alert = false;
$msj = '';
$checkout = false;

if (!empty($_POST)) {
    $venta = new Venta();
    $total = 0;
    $fecha = date('Y-m-d H:G:i');
    $products = json_decode($_POST['products']);
    foreach ($products as $p) {
        $total += floatval($p->precio_vta) * $p->cantidad;
    }
    $venta->total = $total;
    $venta->fecha = $fecha;
    $venta->id_usuario = $user->id_usuario;

    if ($venta->create()) {
        $alert = true;
        $msj = 'Order completed!';
        $checkout = true;

        foreach ($products as $prod) {
            $orden = new Orden();
            $orden->id_venta = $venta->id_venta;
            $orden->precio_vta = $prod->precio_vta;
            $orden->id_producto = $prod->id_producto;
            $orden->cantidad = intval($prod->cantidad);
            if (!$orden->create()) {
                $msj = 'Order Error!';
                $checkout = false;
            }
        }
    }
}