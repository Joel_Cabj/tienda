<?php
$producto = new Producto();
$categoria = new Categoria();
$productos = array();
$categorias = $categoria->getCategorias();

// if (!empty($_POST)) {
//     $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : '';
//     $marca = isset($_POST['marca']) ? $_POST['marca'] : '';
//     $precio_vta = isset($_POST['precio_vta']) ? $_POST['precio_vta'] : 0;
//     $stock = isset($_POST['stock']) ? $_POST['stock'] : 0;
//     $destacado = isset($_POST['destacado']) ? $_POST['destacado'] : 'false';
//     $descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : '';
//     $id_categoria = isset($_POST['id_categoria']) ? $_POST['id_categoria'] : 0;
//     $id_subcategoria = isset($_POST['id_subcategoria']) ? $_POST['id_subcategoria'] : 0;
    
//     $producto = new Producto(0, $nombre, $marca, $precio_vta, $stock, $destacado, $descripcion, $id_categoria, $id_subcategoria);

//     if ($producto->create()) {
//         header('Location: ../../?success=productCreate');
//     } else {
//         header('Location: ../../?codeError=20'); //Error al crear;
//     }
// }

if (!empty($_GET)) {
    if (isset($_GET['idProducto'])){
        $producto->id_producto = intval($_GET['idProducto']);
        $producto->getProducto();
    } elseif (isset($_GET['search'])){
        $productos = $producto->search($_GET['search']);
    } 
    else {
        $productos = $producto->getDestacados();
    }
} else {
    $productos = $producto->getDestacados();
}




// elseif (isset($_GET['idCategoria'])) {
//         $producto->id_categoria = intval($_GET['idCategoria']);
//         if (isset($_GET['idSubcategoria'])) {
//             $producto->id_subcategoria = intval($_GET['idSubcategoria']);
//             $productos = $producto->getProductosSubcategoria();
//         } else {
//             $productos = $producto->getProductosCategoria();
//         }