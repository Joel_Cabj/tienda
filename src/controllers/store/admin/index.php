<?php

function moveImg() {
    global $producto;
    $imagen = $_FILES['image'];
    if ( $imagen["type"] != null && $imagen["tmp_name"] != null) {
        $tipo = str_replace("image/", ".",$imagen["type"]);
        $nombre = rand(100,900) . str_replace(" ", "_", $producto->nombre) . str_replace(" ", "_", $producto->marca);
        $nombreImagen = $nombre.$tipo;
        $direccionImagen = $_SERVER['DOCUMENT_ROOT']."/tienda/assets/img/product/uploads/$nombreImagen";
        move_uploaded_file($imagen["tmp_name"], $direccionImagen);
        $producto->img = $nombreImagen;
        if ($producto->id_producto != 0) {
            $producto->updateImage();
        }
    }
}

if (!empty($_POST)) {
    $form = isset($_POST['form']) ? intval($_POST['form']) : 0;
    // header('Content-type: application/json');
    $response = new stdClass();
    $response->ok = false;
    switch ($form) {
        case 1: #Agregar categoría
            $categoria->categoria = isset($_POST['category_name']) ? $_POST['category_name'] : 'varios';
                if ($categoria->create()) {
                    $categoria->getCategoriaByName();
                    if ($categoria->id_categoria != 0) {
                        $subCategoria->id_categoria = $categoria->id_categoria;
                        $subCategoria->subcategoria = isset($_POST['subcategory_name']) ? $_POST['subcategory_name'] : 'varios';
                        if ($subCategoria->create()) {
                            // $response = new stdClass();
                            $response->ok = true;
                            $response->categoria = $categoria;
                            $response->subcategoria = $subCategoria;
                        }
                    }
                } else {
                    echo "no se creo";
                }
            break;
        case 2: // Agregar Producto
            $producto->nombre = isset($_POST['nombre']) ? $_POST['nombre'] : 'Example';
            $producto->marca = isset($_POST['marca']) ? $_POST['marca'] : 'Example';
            $producto->precio_vta = isset($_POST['precio_vta']) ? floatval($_POST['precio_vta']) : 'Example';
            $producto->stock = isset($_POST['stock']) ? $_POST['stock'] : 0;
            $producto->descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : 'Example';
            $producto->id_categoria = isset($_POST['id_categoria']) ? intval($_POST['id_categoria']) : 0;
            $producto->id_subcategoria = isset($_POST['id_subcategoria']) ? intval($_POST['id_subcategoria']) : 0;
            if ($_FILES['image']) {
                moveImg();
            }
            $producto->create();
        case 3: // Update Producto
            $producto->id_producto = isset($_POST['id_producto']) ? $_POST['id_producto'] : 0;
            $producto->nombre = isset($_POST['nombre']) ? $_POST['nombre'] : 'Example';
            $producto->marca = isset($_POST['marca']) ? $_POST['marca'] : 'Example';
            $producto->precio_vta = isset($_POST['precio_vta']) ? floatval($_POST['precio_vta']) : 'Example';
            $producto->stock = isset($_POST['stock']) ? $_POST['stock'] : 0;
            $producto->estado = isset($_POST['estado']) ? $_POST['estado'] : 1;
            $producto->descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : 'Example';
            $producto->id_categoria = isset($_POST['id_categoria']) ? intval($_POST['id_categoria']) : 0;
            $producto->id_subcategoria = isset($_POST['id_subcategoria']) ? intval($_POST['id_subcategoria']) : 0;
            if ($_FILES['image']) {
                moveImg();
            }
            $producto->update();
            break;
        case 4: // Actualizar Categoria
            $categoria->id_categoria = isset($_POST['id_categoria']) ? $_POST['id_categoria'] : 0;
            $categoria->categoria = isset($_POST['categoria']) ? $_POST['categoria'] : 'example';
            $categoria->update();
            break;
        default:
            # code...
            break;
    }

    // echo json_encode($response);
}