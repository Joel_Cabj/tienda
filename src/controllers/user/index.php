<?php

$alert = false;
$msj = '';

if (!empty($_POST)) {
    $form = isset($_POST['form']) ? intval($_POST['form']) : 0;
    switch ($form) {
        case 11: // update user 
            $user->nombre = isset($_POST['nombre']) ? $_POST['nombre'] : $user->nombre;
            $user->apellido = isset($_POST['apellido']) ? $_POST['apellido'] : $user->apellido;
            $user->email = isset($_POST['email']) ? $_POST['email'] : $user->email;
            $user->direccion = isset($_POST['direccion']) ? $_POST['direccion'] : $user->direccion;
            $user->ciudad = isset($_POST['ciudad']) ? $_POST['ciudad'] : $user->ciudad;
            $user->provincia = isset($_POST['provincia']) ? $_POST['provincia'] : $user->provincia;
            $alert = $user->update();
            if ($alert) {
                $msj = 'User update!';
            }
            break;
        
        default:
            # code...
            break;
    }
}