<?php

$producto = new Producto();
$categoria = new Categoria();
$subCategoria = new Subcategoria();
$categorias = array();
$subCategorias = array();
$productos = array();


if (!empty($_GET)) {
    if (isset($_GET['idCategoria'])){
        $categoria->id_categoria = intval($_GET['idCategoria']);
        $categoria->getCategoria();
        $categorias = $categoria->getCategorias();
        $subCategorias = $categoria->getSubCategorias();
        if (isset($_GET['idSubcategoria'])) {
            $subCategoria->id_subcategoria = intval($_GET['idSubcategoria']);
            $producto->id_categoria = $categoria->id_categoria;
            $producto->id_subcategoria = $subCategoria->id_subcategoria;
            $productos = $producto->getProductosSubcategoria();
        } else {
            $productos = $categoria->getProductos();
        }
    } elseif (isset($_GET['idSubcategoria'])) {
        $categoria->id_categoria = intval($_GET['idCategoria']);
        $subCategoria->id_subcategoria = intval($_GET['idSubcategoria']);
        $subCategoria->getSubCategoria();
        $producto->id_categoria = $categoria->id_categoria;
        $producto->id_subcategoria = $subCategoria->id_subcategoria;
        $productos = $producto->getProductosSubcategoria();
    } elseif (isset($_GET['search'])){
        $categorias = $categoria->search($_GET['search']);
    }
    else {
        $categorias = $categoria->getCategorias();
        $productos = $producto->getDestacados();
    }
} else {
    $categorias = $categoria->getCategorias();
    $productos = $producto->getDestacados();
}