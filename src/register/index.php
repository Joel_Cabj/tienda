<?php
print_r($_POST);
if (!empty($_POST)) {
    require_once '../_bd/bd.php';
    require_once '../clases/Usuario.php';
    $bd = new BD();
    
    if (isset($_POST['email']) && isset($_POST['clave'])) {
        $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : '';
        $apellido = isset($_POST['apellido']) ? $_POST['apellido'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $clave = isset($_POST['clave']) ? $_POST['clave'] : '123456';
        $dni = isset($_POST['dni']) ? $_POST['dni'] : '';
    
        $user = new Usuario(0,$nombre, $apellido, $dni, $email, $clave, 2);
        print_r($user);
        if ($user->create()) {
            // echo "usuario creado";
            header('Location: ../../?notify=1');
        } else {
            // echo "usuario NO creado";
            header('Location: ../../?codeError=10'); // Error al registrar.
        }
    } else {
        // echo "Sin clave/email";
        header('Location: ../../?codeError=11'); // Error de envío de datos.
    }

} else {
    // echo "No post";
    header('Location: ../../');
}