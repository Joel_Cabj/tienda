<?php

// Requiere $Login
$usuario = new Usuario();
$usuarios = array();

if ($Login->getRol() != 'admin') {
    header('Location: ../../');
}

if (!empty($_POST)) {
    $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : '';
    $apellido = isset($_POST['apellido']) ? $_POST['apellido'] : '';
    $dni = isset($_POST['dni']) ? $_POST['dni'] : '';
    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $clave = isset($_POST['clave']) ? $_POST['clave'] : md5('123456');
    $id_rol = isset($_POST['id_rol']) ? $_POST['id_rol'] : 2;
    
    $user = new Usuario($nombre, $apellido, $dni, $email, $clave, $id_rol);

    if ($user->create()) {
        header('Location: ../../admin/users/?success=userCreate');
    } else {
        header('Location: ../../admin/users/?codeError=20'); //Error al crear;
    }
}

if (!empty($_GET)) {
    if (isset($_GET['idUsuario'])){
        $usuario->id_usuario = intval($_GET['idUsuario']);
        $usuario->getUsuario();
    } elseif (isset($_GET['search'])) {
        // buscador
        // header('Location: ../../');
        $usuarios = $usuario->search($_GET['search']);
    } else {
        $usuarios = $usuario->getUsuarios();
        // header('Location: ../../admin/users');
    }
} else {
    $usuarios = $usuario->getUsuarios();
}