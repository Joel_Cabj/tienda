<?php

class Producto {
    public $id_producto, $nombre, $marca, $precio_vta, $stock, $destacado, $descripcion, $id_categoria, $id_subcategoria;
    
    function __construct($id_producto=0, $nombre='', $marca='', $precio_vta=0, $stock=0, $destacado='false', $descripcion='', $id_categoria=0, $id_subcategoria=0) {
        $this->id_producto = $id_producto; 
        $this->nombre = $nombre; 
        $this->marca = $marca; 
        $this->precio_vta = $precio_vta; 
        $this->stock = $stock;
        $this->img = null; // Ver 
        $this->estado = 1;
        $this->destacado = $destacado; 
        $this->descripcion = $descripcion; 
        $this->id_categoria = $id_categoria; 
        $this->id_subcategoria = $id_subcategoria; 
        $this->categoria = '';
        $this->subcategoria = '';
    }


    public function getProducto() {
        global $bd;
        $id = $this->id_producto;
        $sql = "SELECT * FROM productos WHERE id_producto = $id";
        $response = $bd->q($sql)->fetch_assoc();
        if ($response) {
            $this->id_producto = $response['id_producto']; 
            $this->nombre = $response['nombre']; 
            $this->marca = $response['marca']; 
            $this->precio_vta = $response['precio_vta']; 
            $this->stock = $response['stock'];
            $this->img = $response['img']; 
            $this->estado = $response['estado']; 
            $this->destacado = $response['destacado']; 
            $this->descripcion = $response['descripcion']; 
            $this->id_categoria = $response['id_categoria']; 
            $this->id_subcategoria = $response['id_subcategoria']; 
            return true;
        } else {
            return false;
        }
    }

    public function getProductos() {
        global $bd;
        $sql = "SELECT * FROM productos";
        $productos = array();
        $response = $bd->q($sql);
        
        while ($prod = $response->fetch_assoc()) {
            $p = new Producto();
            $p->id_producto = $prod['id_producto']; 
            $p->nombre = $prod['nombre']; 
            $p->marca = $prod['marca']; 
            $p->precio_vta = $prod['precio_vta']; 
            $p->stock = $prod['stock'];
            $p->img = $prod['img'];  
            $p->estado = $prod['estado'];  
            $p->destacado = $prod['destacado']; 
            $p->descripcion = $prod['descripcion']; 
            $p->id_categoria = $prod['id_categoria']; 
            $p->id_subcategoria = $prod['id_subcategoria']; 
            array_push($productos, $p);
        }
        
        return $productos;
    }

    public function getDestacados() {
        global $bd;
        $sql = "SELECT * FROM productos WHERE destacado = 'true'";
        $productos = array();
        $response = $bd->q($sql);
        
        while ($prod = $response->fetch_assoc()) {
            $p = new Producto();
            $p->id_producto = $prod['id_producto']; 
            $p->nombre = $prod['nombre']; 
            $p->marca = $prod['marca']; 
            $p->precio_vta = $prod['precio_vta']; 
            $p->stock = $prod['stock'];
            $p->img = $prod['img'];  
            $p->estado = $prod['estado'];
            $p->destacado = $prod['destacado']; 
            $p->descripcion = $prod['descripcion']; 
            $p->id_categoria = $prod['id_categoria']; 
            $p->id_subcategoria = $prod['id_subcategoria']; 
            array_push($productos, $p);
        }
        
        return $productos;
    }

    public function getProductosCategoria() {
        global $bd;
        $id_categoria = $this->id_categoria;
        $sql = "SELECT * FROM productos WHERE id_categoria = $id_categoria";
        $productos = array();
        $response = $bd->q($sql);
        
        while ($prod = $response->fetch_assoc()) {
            $p = new Producto();
            $p->id_producto = $prod['id_producto']; 
            $p->nombre = $prod['nombre']; 
            $p->marca = $prod['marca']; 
            $p->precio_vta = $prod['precio_vta']; 
            $p->stock = $prod['stock'];
            $p->img = $prod['img']; 
            $p->estado = $prod['estado'];  
            $p->destacado = $prod['destacado']; 
            $p->descripcion = $prod['descripcion']; 
            $p->id_categoria = $prod['id_categoria']; 
            $p->id_subcategoria = $prod['id_subcategoria']; 
            array_push($productos, $p);
        }
        
        return $productos;
    }

    public function getProductosSubcategoria() {
        global $bd;
        $id_categoria = $this->id_categoria;
        $id_subcategoria = $this->id_subcategoria;
        $sql = "SELECT * FROM productos WHERE id_categoria = $id_categoria AND id_subcategoria = $id_subcategoria";
        $productos = array();
        $response = $bd->q($sql);
        
        while ($prod = $response->fetch_assoc()) {
            $p = new Producto();
            $p->id_producto = $prod['id_producto']; 
            $p->nombre = $prod['nombre']; 
            $p->marca = $prod['marca']; 
            $p->precio_vta = $prod['precio_vta']; 
            $p->stock = $prod['stock']; 
            $p->img = $prod['img']; 
            $p->estado = $prod['estado'];  
            $p->destacado = $prod['destacado']; 
            $p->descripcion = $prod['descripcion']; 
            $p->id_categoria = $prod['id_categoria']; 
            $p->id_subcategoria = $prod['id_subcategoria']; 
            array_push($productos, $p);
        }
        
        return $productos;
    }

    public function create(){
        global $bd;
        $nombre = $this->nombre; 
        $marca = $this->marca; 
        $precio_vta = $this->precio_vta; 
        $stock = $this->stock;
        $img = isset($this->img) ? $this->img : 'no-img.png'; 
        $destacado = $this->destacado; 
        $descripcion = isset($this->descripcion) ? $this->descripcion : 'Without description'; 
        $id_categoria = $this->id_categoria; 
        $id_subcategoria = $this->id_subcategoria; 
        $sql = "INSERT INTO productos (nombre, marca, precio_vta, stock, img, destacado, descripcion, id_categoria, id_subcategoria) VALUES ('$nombre', '$marca', $precio_vta, $stock, '$img' , '$destacado', '$descripcion', $id_categoria, $id_subcategoria)";
        // echo $sql;
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $bd;
        $id = $this->id_producto;
        $nombre = $this->nombre; 
        $marca = $this->marca; 
        $precio_vta = $this->precio_vta; 
        $stock = $this->stock; 
        $destacado = $this->destacado; 
        $estado = $this->estado;
        $descripcion = isset($this->descripcion) ? $this->descripcion : 'Without description'; 
        $id_categoria = $this->id_categoria; 
        $id_subcategoria = $this->id_subcategoria; 
        $sql = "UPDATE productos SET nombre = '$nombre', marca = '$marca', precio_vta = $precio_vta, stock = $stock, estado = $estado, destacado = '$destacado', descripcion = '$descripcion', id_categoria = $id_categoria, id_subcategoria = $id_subcategoria WHERE id_producto = $id";
        // echo $sql;
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function updateImage() {
        global $bd;
        $id = $this->id_producto;
        $img = isset($this->img) ? $this->img : 'no-img.png';  
        $sql = "UPDATE productos SET img = '$img' WHERE id_producto = $id";
        // echo $sql;
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function decreaseStock(){
        global $bd;
        $id = $this->id_producto;
        $stock = $this->stock - 1;
        $sql = "UPDATE productos SET stock = $stock WHERE id_producto = $id";
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function getCategoria() {
        global $bd;
        $categoria = '';
        $id_categoria = $this->id_categoria;
        $sql = "SELECT * FROM categorias WHERE id_categoria = $id_categoria";
        $response = $bd->q($sql)->fetch_assoc();
        if ($response) {
            $categoria = $response['categoria']; 
        }

        return $categoria;
    }

    public function getSubCategoria() {
        global $bd;
        $subcategoria = '';
        $id_subcategoria = $this->id_subcategoria;
        $sql = "SELECT * FROM subcategorias WHERE id_subcategoria = $id_subcategoria";
        $response = $bd->q($sql)->fetch_assoc();
        if ($response) {
            $subcategoria = $response['subcategoria']; 
        }

        return $subcategoria;
    }

    public function search($search) {
        global $bd;
        $search = $bd->real_escape_string($search);
        $sql = "SELECT * FROM productos WHERE nombre LIKE '%$search%' OR marca LIKE '%$search%' or descripcion LIKE '%$search%'";
        $productos = array();
        $response = $bd->q($sql);
        
        while ($prod = $response->fetch_assoc()) {
            $p = new Producto();
            $p->id_producto = $prod['id_producto']; 
            $p->nombre = $prod['nombre']; 
            $p->marca = $prod['marca']; 
            $p->precio_vta = $prod['precio_vta']; 
            $p->stock = $prod['stock']; 
            $p->img = $prod['img'];
            $p->destacado = $prod['destacado']; 
            $p->descripcion = $prod['descripcion']; 
            $p->id_categoria = $prod['id_categoria']; 
            $p->id_subcategoria = $prod['id_subcategoria']; 
            array_push($productos, $p);
        }
        
        return $productos;
    }

    public function hotProduct() {
        global $bd;
        $id = $this->id_producto;
        $sql = "SELECT * FROM productos WHERE id_producto = $id";
        $response = $bd->q($sql)->fetch_assoc();
        if ($response) {
            $destacado = $response['destacado'] === 'true' ? 'false' : 'true'; 
            $this->destacado = $destacado;
            $update = "UPDATE productos SET destacado = '$destacado' WHERE id_producto = $id";
            if ($bd->q($update)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}