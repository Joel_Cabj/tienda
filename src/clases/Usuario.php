<?php
// require_once('../_bd/bd.php');
// $bd = new BD();

class Usuario 
{
    public $id_usuario, $nombre, $apellido, $dni, $email, $clave, $id_rol, $ciudad, $provincia, $direccion; 
    
    function __construct($id_usuario=0, $nombre='', $apellido='', $dni='', $email='', $clave='', $id_rol=2) {
        $this->id_usuario = $id_usuario;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->dni = $dni;
        $this->email = $email;
        $this->direccion;
        $this->ciudad;
        $this->provincia;
        $this->clave = $clave;
        $this->id_rol = $id_rol;
    }

    public function getUsuario() {
        global $bd;
        $id = $this->id_usuario;
        $sql = "SELECT * FROM usuarios WHERE id_usuario = $id";
        $response = $bd->q($sql)->fetch_assoc();
        if ($response) {
            $this->id_usuario = $response['id_usuario'];
            $this->nombre = $response['nombre'];
            $this->apellido = $response['apellido'];
            $this->dni = $response['dni'];
            $this->email = $response['email'];
            $this->direccion = $response['direccion'];
            $this->ciudad = $response['ciudad'];
            $this->provincia = $response['provincia'];
            $this->id_rol = $response['id_rol'];
            return $this;
        } else {
            return null;
        }
    }

    public function getRol() {
        global $bd;
        $id = $this->id_rol;
        $sql = "SELECT * FROM roles WHERE id_rol = $id";
        $rol = 'guest';
        $response = $bd->q($sql)->fetch_assoc();
        if ($response) {
            $rol = $response['rol'];
        } 

        return $rol;
    }

    public function getUsuarios() {
        global $bd;
        $sql = "SELECT * FROM usuarios";
        $usuarios = array();
        $response = $bd->q($sql);
        
        while ($usuario = $response->fetch_assoc()) {
            $user = new Usuario();
            $user->id_usuario = $usuario['id_usuario'];
            $user->nombre = $usuario['nombre'];
            $user->apellido = $usuario['apellido'];
            $user->dni = $usuario['dni'];
            $user->email = $usuario['email'];
            $user->id_rol = $usuario['id_rol'];
            array_push($usuarios, $user);
        }
        
        return $usuarios;
    }

    public function create(){
        global $bd;
        $nombre = $this->nombre;
        $apellido = $this->apellido;
        $dni = $this->dni;
        $email = $this->email;
        $clave = md5($this->clave);
        $id_rol = $this->id_rol;
        $sql = "INSERT INTO usuarios (nombre, apellido, email, clave, id_rol) VALUES ('$nombre', '$apellido', '$email', '$clave', $id_rol)";
        echo $sql;
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $bd;
        $id = $this->id_usuario;
        $nombre = $this->nombre;
        $apellido = $this->apellido;
        $dni = $this->dni;
        $direccion = $this->direccion;
        $ciudad = $this->ciudad;
        $provincia = $this->provincia;
        $email = $this->email;
        $id_rol = $this->id_rol;
        $sql = "UPDATE usuarios SET nombre = '$nombre', apellido = '$apellido', dni = '$dni', email = '$email', direccion = '$direccion', ciudad = '$ciudad', provincia = '$provincia' WHERE id_usuario = $id";
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }   

    public function deleteCargo() {
        global $bd;
        $id = $this->id_cargo;
        $sql = "DELETE FROM usuarios where id_usuario = $id";
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function getCompras() {
        global $bd;
        $id = $this->id_usuario;
        $sql = "SELECT * FROM ventas WHERE id_usuario = $id";
        $compras = array();
        $response = $bd->q($sql);
        
        while ($compra = $response->fetch_assoc()) {
            $c = new Venta();
            $c->id_venta = $compra['id_venta'];
            $c->total = $compra['total'];
            $c->fecha = $compra['fecha'];
            $c->id_usuario = $compra['id_usuario'];
            array_push($compras, $c);
        }
        
        return $compras;
    }

    public function search($search) {
        global $bd;
        $search = $bd->real_escape_string($search);
        $sql = "SELECT * FROM usuarios WHERE nombre LIKE '$search' OR apellido LIKE '$search' OR email LIKE '$search' or dni LIKE '$search'";
        $usuarios = array();
        $response = $bd->q($sql);
        
        while ($usuario = $response->fetch_assoc()) {
            $user = new Usuario();
            $user->id_usuario = $usuario['id_usuario'];
            $user->nombre = $usuario['nombre'];
            $user->apellido = $usuario['apellido'];
            $user->dni = $usuario['dni'];
            $user->email = $usuario['email'];
            $user->id_rol = $usuario['id_rol'];
            array_push($usuarios, $user);
        }
        
        return $usuarios;
    }
}