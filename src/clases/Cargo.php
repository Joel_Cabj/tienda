<?php

require('../_bd/bd.php');
$bd = new BD();

class Cargo 
{
    // Declaro las variables de la clase como publicas, para no andar creado funciones para obtener/setear los datos xD
    public $id_cargo, $cargo, $id_superior;
    
    // El constructo sirve para setear parametros cuando se crea un Objeto Cargo --> new Cargo();
    function __construct ($cargo='', $superior=0, $id=0) {
        $this->id_cargo = $id;
        $this->cargo = $cargo;
        $this->id_superior = $superior;
    }
    
    // Funcion para obtener un unico cargo de la BD a través del $id
    public function getCargo($id) {
        global $bd;
        $sql = "SELECT * FROM cargos WHERE id_cargo = $id";
        $response = $bd->q($sql)->fetch_assoc();
        if ($response) {
            $this->id_cargo = $response['id_cargo'];
            $this->cargo = $response['cargo'];
            $this->id_superior = $response['id_superior'];
            return $this;
        } else {
            return null;
        }
    }

    // Funcion para obtener todos los cargos (o un array vacio) de la BD.
    public function getCargos() {
        global $bd;
        $sql = "SELECT * FROM cargos";
        $cargos = array();
        $response = $bd->q($sql);
        
        while ($cargo = $response->fetch_assoc()) {
            $c = new Cargo();
            $c->id_cargo = $cargo['id_cargo'];
            $c->cargo = $cargo['cargo'];
            $c->id_superior = $cargo['id_superior'];
            array_push($cargos, $c);
        }
        
        return $cargos;
    }
    
    // Funcion para crear un cargo en la BD.
    public function createCargo(){
        global $bd;
        $cargo = $this->cargo;
        $superior = $this->id_superior;
        $sql = "INSERT INTO cargos (cargo, id_superior) VALUES ('$cargo', $superior)";
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }

    // Funcion para actuatilizar un cargo
    public function updateCargo() {
        global $bd;
        $cargo = $this->cargo;
        $id = $this->id_cargo;
        $superior = $this->id_superior;
        $sql = "UPDATE cargo SET cargo = '$cargo', id_superior = $superior WHERE id_cargo = $id";
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }   

    // Funcion para borrar un Cargo
    public function deleteCargo() {
        global $bd;
        $id = $this->id_cargo;
        $sql = "DELETE FROM cargos where id_cargo = $id";
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }
}

$obj = new Cargo();
$obj->getCargos();
print_r($obj->getCargo(1));

