<?php

// require('../_bd/bd.php');
// $bd = new BD();

class Categoria {
    public $id_categoria, $categoria;

    function __construct ($id_categoria=0, $categoria='') {
        $this->id_categoria = $id_categoria;
        $this->categoria = $categoria;
    }

    public function getCategoria() {
        global $bd;
        $id = $this->id_categoria;
        $sql = "SELECT * FROM categorias WHERE id_categoria = $id";
        $response = $bd->q($sql)->fetch_assoc();
        if ($response) {
            $this->categoria = $response['categoria']; 
            $this->id_categoria = $response['id_categoria']; 
            return $this;
        } else {
            return null;
        }
    }

    public function getCategoriaByName() {
        global $bd;
        $nombre = $this->categoria;
        $sql = "SELECT * FROM categorias WHERE categoria = '$nombre'";
        $response = $bd->q($sql)->fetch_assoc();
        if ($response) {
            $this->categoria = $response['categoria']; 
            $this->id_categoria = $response['id_categoria']; 
            return $this;
        } else {
            return null;
        }
    }

    public function getCategorias() {
        global $bd;
        $sql = "SELECT * FROM categorias";
        $categorias = array();
        $response = $bd->q($sql);
        
        while ($cat = $response->fetch_assoc()) {
            $c = new Categoria();
            $c->categoria = $cat['categoria']; 
            $c->id_categoria = $cat['id_categoria']; 
            array_push($categorias, $c);
        }
        
        return $categorias;
    }

    public function getProductos() {
        global $bd;
        $id = $this->id_categoria;
        $sql = "SELECT * FROM productos WHERE id_categoria = $id AND estado = 1";
        $productos = array();
        $response = $bd->q($sql);
        
        while ($prod = $response->fetch_assoc()) {
            $p = new stdClass();
            $p->id_producto = $prod['id_producto']; 
            $p->nombre = $prod['nombre']; 
            $p->marca = $prod['marca']; 
            $p->precio_vta = $prod['precio_vta']; 
            $p->stock = $prod['stock']; 
            $p->img = $prod['img']; 
            $p->destacado = $prod['destacado']; 
            $p->descripcion = $prod['descripcion']; 
            $p->id_categoria = $prod['id_categoria']; 
            $p->id_subcategoria = $prod['id_subcategoria']; 
            array_push($productos, $p);
        }
        
        return $productos;
    }

    // estado 1 and 0
    public function getAllProductos() {
        global $bd;
        $id = $this->id_categoria;
        $sql = "SELECT * FROM productos WHERE id_categoria = $id";
        $productos = array();
        $response = $bd->q($sql);
        
        while ($prod = $response->fetch_assoc()) {
            $p = new stdClass();
            $p->id_producto = $prod['id_producto']; 
            $p->nombre = $prod['nombre']; 
            $p->marca = $prod['marca']; 
            $p->precio_vta = $prod['precio_vta']; 
            $p->stock = $prod['stock']; 
            $p->img = $prod['img']; 
            $p->destacado = $prod['destacado']; 
            $p->descripcion = $prod['descripcion']; 
            $p->id_categoria = $prod['id_categoria']; 
            $p->id_subcategoria = $prod['id_subcategoria']; 
            array_push($productos, $p);
        }
        
        return $productos;
    }

    public function create(){
        global $bd;
        $categoria = $this->categoria; 
        $sql = "INSERT INTO categorias (categoria) VALUES ('$categoria')";
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $bd;
        $id = $this->id_categoria;
        $categoria = $this->categoria; 
        $sql = "UPDATE categorias SET categoria = '$categoria' WHERE id_categoria = $id";
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function getSubCategorias() {
        global $bd;
        $id = $this->id_categoria;
        $sql = "SELECT * FROM subcategorias WHERE id_categoria = $id";
        $subcategorias = array();
        $response = $bd->q($sql);
        
        while ($cat = $response->fetch_assoc()) {
            $c = new stdClass();
            $c->id_subcategoria = $cat['id_subcategoria']; 
            $c->subcategoria = $cat['subcategoria']; 
            $c->id_categoria = $cat['id_categoria']; 
            array_push($subcategorias, $c);
        }
        
        return $subcategorias;
    }

    public function search($search) {
        global $bd;
        $search = $bd->real_escape_string($search);
        $sql = "SELECT * FROM categorias WHERE categoria LIKE '%$search%'";
        $categorias = array();
        $response = $bd->q($sql);
        
        while ($cat = $response->fetch_assoc()) {
            $c = new Categoria();
            $c->categoria = $cat['categoria']; 
            $c->id_categoria = $cat['id_categoria']; 
            array_push($categorias, $c);
        }
        
        return $categorias;
    }

    public function gethotProducts(){
        global $bd;
        $id = $this->id_categoria;
        $sql = "SELECT * FROM productos WHERE id_categoria = $id AND destacado = 'true' AND estado = 1";
        $productos = array();
        $response = $bd->q($sql);
        
        while ($prod = $response->fetch_assoc()) {
            $p = new stdClass();
            $p->id_producto = $prod['id_producto']; 
            $p->nombre = $prod['nombre']; 
            $p->marca = $prod['marca']; 
            $p->precio_vta = $prod['precio_vta']; 
            $p->stock = $prod['stock']; 
            $p->img = $prod['img']; 
            $p->destacado = $prod['destacado']; 
            $p->descripcion = $prod['descripcion']; 
            $p->id_categoria = $prod['id_categoria']; 
            $p->id_subcategoria = $prod['id_subcategoria']; 
            array_push($productos, $p);
        }

        return $productos;
    }
}

// $cat = new Categoria(1);
// print_r($cat->getSubCategorias());
