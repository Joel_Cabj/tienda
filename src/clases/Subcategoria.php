<?php

// require('../_bd/bd.php');
// $bd = new BD();

class Subcategoria {
    public $id_subcategoria, $subcategoria, $id_categoria;

    function __construct ($id_subcategoria=0, $subcategoria='', $id_categoria=0) {
        $this->id_subcategoria = $id_subcategoria;
        $this->subcategoria = $subcategoria;
        $this->id_categoria = $id_categoria;
    }

    public function getSubCategoria() {
        $id = $this->id_subcategoria;
        $sql = "SELECT * FROM subcategorias WHERE id_subcategoria = $id";
        $response = $bd->q($sql)->fetch_assoc();
        if ($response) {
            $this->id_subcategoria = $response['id_subcategoria']; 
            $this->subcategoria = $response['subcategoria']; 
            $this->id_categoria = $response['id_categoria']; 
            return $this;
        } else {
            return null;
        }
    }

    public function getSubcategorias() {
        global $bd;
        $sql = "SELECT * FROM subcategorias";
        $subcategorias = array();
        $response = $bd->q($sql);
        
        while ($cat = $response->fetch_assoc()) {
            $s = new Subcategoria();
            $s->id_subcategoria = $cat['id_subcategoria']; 
            $s->subcategoria = $cat['subcategoria']; 
            $s->id_categoria = $cat['id_categoria']; 
            array_push($subcategorias, $s);
        }
        
        return $subcategorias;
    }

    public function create(){
        global $bd;
        $subcategoria = $this->subcategoria; 
        $id_categoria = $this->id_categoria; 
        $sql = "INSERT INTO subcategorias (subcategoria, id_categoria) VALUES ('$subcategoria', $id_categoria)";
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete(){
        global $bd;
        $id = $this->id_subcategoria; 
        $sql = "DELETE FROM subcategorias WHERE id_subcategoria = $id";
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $bd;
        $id = $this->id_subcategoria;
        $subcategoria = $this->subcategoria; 
        $sql = "UPDATE subcategorias SET subcategoria = '$subcategoria' WHERE id_subcategoria = $id";
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function getAssignedProducts() {
        global $bd;
        $id = $this->id_subcategoria;
        $sql = "SELECT * FROM productos WHERE id_subcategoria = $id";
        $productos = array();
        $response = $bd->q($sql);
        
        while ($prod = $response->fetch_assoc()) {
            $p = new stdClass();
            $p->id_producto = $prod['id_producto']; 
            $p->nombre = $prod['nombre']; 
            $p->marca = $prod['marca']; 
            $p->precio_vta = $prod['precio_vta']; 
            $p->stock = $prod['stock']; 
            $p->img = $prod['img']; 
            $p->destacado = $prod['destacado']; 
            $p->descripcion = $prod['descripcion']; 
            $p->id_categoria = $prod['id_categoria']; 
            $p->id_subcategoria = $prod['id_subcategoria']; 
            array_push($productos, $p);
        }

        return $productos;
    }

    public function gethotProducts(){
        global $bd;
        $id = $this->id_subcategoria;
        $sql = "SELECT * FROM productos WHERE id_subcategoria = $id AND destacado = 'true'";
        $productos = array();
        $response = $bd->q($sql);
        
        while ($prod = $response->fetch_assoc()) {
            $p = new stdClass();
            $p->id_producto = $prod['id_producto']; 
            $p->nombre = $prod['nombre']; 
            $p->marca = $prod['marca']; 
            $p->precio_vta = $prod['precio_vta']; 
            $p->stock = $prod['stock']; 
            $p->img = $prod['img']; 
            $p->destacado = $prod['destacado']; 
            $p->descripcion = $prod['descripcion']; 
            $p->id_categoria = $prod['id_categoria']; 
            $p->id_subcategoria = $prod['id_subcategoria']; 
            array_push($productos, $p);
        }

        return $productos;
    }

}

// $cat = new Subcategoria(0, 'LED', 1);
// print_r($cat->create());