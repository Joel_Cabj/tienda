<?php
class Orden {
    public  $id_producto, $cantidad, $precio_vta, $id_venta;

    function __construct($id_producto=0, $cantidad=0, $precio_vta=0, $id_venta=0){
        $this->id_producto = $id_producto;
        $this->cantidad = $cantidad;
        $this->precio_vta = $precio_vta;
        $this->id_venta = $id_venta;
    }

    public function create(){
        global $bd;
        $cantidad = $this->cantidad;
        $precio_vta = $this->precio_vta;
        $id_venta = $this->id_venta;
        $id_producto  = $this->id_producto;
        $sql = "INSERT INTO ordenes (cantidad, precio_vta, id_venta, id_producto) VALUES ($cantidad, $precio_vta, $id_venta, $id_producto)";
        if ($bd->q($sql)) {
            return true;
        } else {
            return false;
        }
    }
    
}