<?php

class Venta {
    public $id_venta, $total, $fecha, $id_usuario;

    function __construct($id_venta=0, $total=0, $fecha='', $id_usuario=0){
        $this->id_venta = $id_venta;
        $this->total = $total;
        $this->fecha = $fecha;
        $this->id_usuario = $id_usuario;
    }

    public function getVenta($id) {
        global $bd;
        $sql = "SELECT * FROM ventas WHERE id_venta = $id";
        $response = $bd->q($sql)->fetch_assoc();
        if ($response) {
            $this->id_venta = $response['id_venta'];
            $this->total = $response['total'];
            $this->fecha = $response['fecha'];
            $this->id_usuario = $response['id_usuario'];
            return $this;
        } else {
            return null;
        }
    }

    public function getVentas() {
        global $bd;
        $sql = "SELECT * FROM ventas";
        $ventas = array();
        $response = $bd->q($sql);
        
        while ($venta = $response->fetch_assoc()) {
            $v = new Venta();
            $v->id_venta = $venta['id_venta'];
            $v->total = $venta['total'];
            $v->fecha = $venta['fecha'];
            $v->id_usuario = $venta['id_usuario'];
            array_push($ventas, $v);
        }
        
        return $ventas;
    }

    public function create(){
        global $bd;
        $total = $this->total;
        $fecha = $this->fecha;
        $id_usuario = $this->id_usuario;
        $sql = "INSERT INTO ventas (total, fecha, id_usuario) VALUES ($total, '$fecha', $id_usuario);";
        if ($bd->q($sql)) {
            $idSQL = "SELECT LAST_INSERT_ID() as id;";
            $id_venta = $bd->q($idSQL)->fetch_assoc();
            $this->id_venta = intval($id_venta['id']);
            return true;
        } else {
            return false;
        }
    }

    public function getProductos() {
        global $bd;
        $id = $this->id_venta;
        $sql = "SELECT * FROM ordenes WHERE id_venta = $id";
        $productos = array();
        $response = $bd->q($sql);
        
        while ($prod = $response->fetch_assoc()) {
            $c = new stdClass();
            $c->id_producto = $prod['id_producto']; 
            $c->cantidad = $prod['cantidad']; 
            $c->precio_vta = $prod['precio_vta']; 
            $c->id_venta = $prod['id_venta']; 
            array_push($productos, $c);
        }
        
        return $productos;
    }
}