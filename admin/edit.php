<?php
 $CONFIGS = include("../config.php");
 list($SRC, $COMPONENTS, $ASSETS) = $CONFIGS;
 require_once($SRC."_bd/bd.php");
 require_once($SRC."login/Login.php");
 require_once($SRC."clases/Categoria.php");
 require_once($SRC."clases/Producto.php");
 require_once($SRC."clases/Subcategoria.php");
 $categorias = array();
 $subcategorias = array();
 $bd = new BD();
 $Login = new Login();
 $categoria = new Categoria();
 $subcategoria = new Subcategoria();

?>

<?php if ($Login->getRol() === 'admin'): ?>
    <!DOCTYPE html>
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>CORSAIR | ADMIN</title>
        <link rel="icon" href="<?= $ASSETS ?>img/logo_mobile_white.svg">
        <meta name="theme-color" content="#231F20">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/common.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/admin.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
        <?php require_once($COMPONENTS."common/header.component.php") ?>
        <?php require_once($COMPONENTS."common/sidemenu.component.php") ?>
        <?php require_once($COMPONENTS."common/user.component.php") ?>

        <main>
        <?php 
        
        $type = $_GET['type'];
        $id = isset($_GET['id']) ? $_GET['id'] : '0';
        $alert = false;
        $msj = '';
        
        if ($type === 'product') {
            $categorias = $categoria->getCategorias();
            $producto = new Producto;
            $producto->id_producto = $id;
            $producto->getProducto();
            $categoria->id_categoria = $producto->id_categoria;
            $subcategorias = $categoria->getSubCategorias();
            include($COMPONENTS."admin/edit-product.component.php");
        } elseif ($type === 'category') {
            $categoria = new Categoria();
            if (!empty($_POST)) {
                $form = isset($_POST['form']) ? intval($_POST['form']) : 0;
                switch ($form) {
                    case 4: // Actualizar categoria
                        $categoria->id_categoria = isset($_POST['id_categoria']) ? intval($_POST['id_categoria']) : 0;
                        $categoria->categoria = isset($_POST['categoria']) ? $_POST['categoria'] : 'example';
                        $alert = $categoria->update();
                        $msj = 'Category Updated!';
                        break;
                    case 5: // Agregar Subcategoria
                        $subcategoria->id_categoria = isset($_POST['id_categoria']) ? intval($_POST['id_categoria']) : 0;
                        $subcategoria->subcategoria = isset($_POST['subcategory_name']) ? $_POST['subcategory_name'] : 'example';
                        $alert = $subcategoria->create();
                        $msj = "Subcategory created!";
                    case 6: // Edit subcategory 
                        $subcategoria->id_subcategoria = isset($_POST['id_subcategoria']) ? $_POST['id_subcategoria'] : 0;
                        $subcategoria->subcategoria = isset($_POST['subcategory_name_edit']) ? $_POST['subcategory_name_edit'] : 'example';
                        $alert = $subcategoria->update();
                        $msj = 'Subcategory Updated!';
                        break;
                    case 7:
                        $subcategoria->id_subcategoria = isset($_POST['id_subcategoria']) ? $_POST['id_subcategoria'] : 0;
                        $alert = $subcategoria->delete();
                        $msj = "Subcategory deleted!";
                        break;
                    default:
                        # code...
                        break;
                }
                
            }
            $categoria->id_categoria = $id;
            $categoria->getCategoria();
            $subcategorias = $categoria->getSubCategorias();
            include($COMPONENTS."admin/edit-category.component.php");
            include($COMPONENTS."admin/add-subcategory-modal.component.php");
            include($COMPONENTS."admin/edit-subcategory-modal.component.php");
            include($COMPONENTS."admin/remove-subcategory-modal.component.php");
        } else {
            # idk how to handle this
        }
        ?>
        </main>

        <?php require_once($COMPONENTS."common/footer.component.php") ?>
        
        <script type="text/javascript" src="<?= $ASSETS ?>js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/materialize.min.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/common.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/store.admin.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/edit.admin.js"></script>
        <?php if ($alert): ?>
            <script>
                M.toast({ html: '<?= $msj ?>' });
            </script>
        <?php endif; ?>    
    </body>
    </html>
<?php else: ?>
    <?php header('Location: ../'); ?>
<?php endif; ?>