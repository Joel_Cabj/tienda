<?php
 $CONFIGS = include("../config.php");
 list($SRC, $COMPONENTS, $ASSETS) = $CONFIGS;
 require_once($SRC."_bd/bd.php");
 require_once($SRC."login/Login.php");
 $Login = new Login();

?>

<?php 

if ($Login->getRol() === 'admin'):
    $bd = new bd();
    require_once($SRC."clases/Producto.php");
    require_once($SRC."clases/Categoria.php");
    require_once($SRC."clases/Subcategoria.php");
    $producto = new Producto();
    $categoria = new Categoria();
    $subCategoria = new Subcategoria();
    require_once($SRC."controllers/store/admin/index.php");    
    $subCategorias = array();
    $productos = $producto->getProductos();
    $categorias = $categoria->getCategorias();
?>

    <!DOCTYPE html>
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>CORSAIR | ADMIN</title>
        <link rel="icon" href="<?= $ASSETS ?>img/logo_mobile_white.svg">
        <meta name="theme-color" content="#231F20">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/common.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/admin.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/store.admin.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
        <?php require_once($COMPONENTS."common/header.component.php") ?>
        <?php require_once($COMPONENTS."common/sidemenu.component.php") ?>
        <?php require_once($COMPONENTS."common/user.component.php") ?>
        <main>
            <div class="row">
                <div class="col s12 tabs-col">
                    <ul class="tabs">
                        <li class="tab col s6"><a class="active black-text" href="#products">PRODUCTS</a></li>
                        <li class="tab col s6"><a class="black-text" href="#categories">CATEGORIES</a></li>
                    </ul>
                </div>
                <div id="products" class="col s12">
                    <?php include($COMPONENTS."admin/product-table.component.php") ?>
                    <?php include($COMPONENTS."admin/product-overview.component.php") ?>
                </div>
                <div id="categories" class="col s12">
                    <?php include($COMPONENTS."admin/category-table.component.php") ?>
                    <?php include($COMPONENTS."admin/category-overview.component.php") ?>
                </div>
            </div>
            <?php require($COMPONENTS."admin/floating-buttons.component.php") ?>
        </main>
        <?php require_once($COMPONENTS."common/footer.component.php") ?>
        
        <script type="text/javascript" src="<?= $ASSETS ?>js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/materialize.min.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/common.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/store.admin.js"></script>    
    </body>
    </html>
<?php else: ?>
    <?php header('Location: ../'); ?>
<?php endif; ?>