<?php
 $CONFIGS = include("../config.php");
 list($SRC, $COMPONENTS, $ASSETS) = $CONFIGS;
 require_once($SRC."_bd/bd.php");
 require_once($SRC."login/Login.php");
 $Login = new Login();
?>

<?php if ($Login->getRol() === 'admin'): ?>
    <!DOCTYPE html>
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>CORSAIR | ADMIN</title>
        <link rel="icon" href="<?= $ASSETS ?>img/logo_mobile_white.svg">
        <meta name="theme-color" content="#231F20">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/common.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/admin.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
        <?php require_once($COMPONENTS."common/header.component.php") ?>
        <?php require_once($COMPONENTS."common/sidemenu.component.php") ?>
        <?php require_once($COMPONENTS."common/user.component.php") ?>
        <main>
            <div class="row">
                <div class="col s12 offset-m3 m6 offset-l3 l6">
                    <div class="card">
                        <div class="card-image">
                            <img src="<?= $ASSETS ?>img/banner_hero.jpg">
                            <span class="card-title">Corsair Rocks!</span>
                            <a class="btn-floating halfway-fab waves-effect waves-light yellow accent-4"><i class="material-icons black-text">thumb_up_alt</i></a>
                        </div>
                        <div class="card-content">
                            <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                        </div>
                    </div>
                </div>
                <div class="col s12 offset-m3 m6 offset-l3 l6">
                    <div class="card">
                        <div class="card-image">
                            <img src="<?= $ASSETS ?>img/banner_mouse.jpg">
                            <span class="card-title">New Product Comming</span>
                            <a class="btn-floating halfway-fab waves-effect waves-light yellow accent-4"><i class="material-icons black-text">thumb_up_alt</i></a>
                        </div>
                        <div class="card-content">
                            <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                        </div>
                    </div>
                </div>
                <div class="col s12 offset-m3 m6 offset-l3 l6">
                    <div class="card">
                        <div class="card-image">
                            <img src="<?= $ASSETS ?>img/banner_rgb_case.jpg">
                            <span class="card-title">RGB is trending</span>
                            <a class="btn-floating halfway-fab waves-effect waves-light yellow accent-4"><i class="material-icons black-text">thumb_up_alt</i></a>
                        </div>
                        <div class="card-content">
                            <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        
        <?php require_once($COMPONENTS."common/footer.component.php") ?>
        
        <script type="text/javascript" src="<?= $ASSETS ?>js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/materialize.min.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/common.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/admin.js"></script>    
    </body>
    </html>
<?php else: ?>
    <?php header('Location: ../'); ?>
<?php endif; ?>


