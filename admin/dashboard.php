<?php
 $CONFIGS = include("../config.php");
 list($SRC, $COMPONENTS, $ASSETS) = $CONFIGS;
 require_once($SRC."_bd/bd.php");
 require_once($SRC."login/Login.php");
 $Login = new Login();
?>

<?php if ($Login->getRol() === 'admin'): ?>
    <?php
    require($SRC."clases/Venta.php");
    require($SRC."clases/Orden.php");
    $bd = new bd();
    $sells = array();
    $sell = new Venta();
    $sells = $sell->getVentas();
    ?>
    <!DOCTYPE html>
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>CORSAIR | ADMIN</title>
        <link rel="icon" href="<?= $ASSETS ?>img/logo_mobile_white.svg">
        <meta name="theme-color" content="#231F20">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/common.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/admin.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
        <?php require_once($COMPONENTS."common/header.component.php") ?>
        <?php require_once($COMPONENTS."common/sidemenu.component.php") ?>
        <?php require_once($COMPONENTS."common/user.component.php") ?>
        <main>
            <div class="row">
                <div class="col s8 offset-s2">
                    <table class="highlight">
                        <thead>
                            <tr>
                            <th class="center-align">#</th>
                            <th class="center-align">Client ID</th>
                            <th class="center-align">Date</th>
                            <th class="center-align">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($sells as $sell) : ?>
                        <tr>
                            <td class="center-align"><?php echo("<b>".$sell->id_venta."</b>") ?></td>
                            <td class="center-align"><?php echo("<b>".$sell->id_usuario."</b>") ?></td>
                            <td class="center-align"><?php echo("<b>".$sell->fecha."</b>") ?></td>
                            <td class="center-align"><?php echo("<b>$".$sell->total."</b>"); ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                    </table>
                </div>

                <div class="col s12 l6">
                    <h3 class="center">Total Sells</h3>
                    <p class="center">First Quarter (Q1) 2020</p>
                    <canvas id="totalSells"></canvas>
                </div>

                <div class="divider"></div>

                <div class="col s12  l6">
                    <h3 class="center">Sells by Category</h3>
                    <p class="center">First Quarter (Q1) 2020</p>
                    <canvas id="byCategory"></canvas>
                </div>
            </div>
        </main>
        <?php require_once($COMPONENTS."common/footer.component.php") ?>
        
        <script type="text/javascript" src="<?= $ASSETS ?>js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/materialize.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha256-R4pqcOYV8lt7snxMQO/HSbVCFRPMdrhAFMH+vr9giYI=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/common.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/dashboard.js"></script>    
    </body>
    </html>
<?php else: ?>
    <?php header('Location: ../'); ?>
<?php endif; ?>