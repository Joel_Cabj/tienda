<?php
 $CONFIGS = include("../config.php");
 list($SRC, $COMPONENTS, $ASSETS) = $CONFIGS;
 require_once($SRC."login/Login.php");
 $Login = new Login();
 
 if ($Login->getRol() === 'admin'):
    header('Location: ../admin/');
 else:   
    
?>
    <!DOCTYPE html>
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>CORSAIR | STORE</title>
        <link rel="icon" href="<?= $ASSETS ?>img/logo_mobile_black.svg">
        <meta name="theme-color" content="#ffffff">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/common.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/store.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
        <?php require_once($COMPONENTS."common/header.component.php") ?>
        <?php require_once($COMPONENTS."common/sidemenu.component.php") ?>
        <?php require_once($COMPONENTS."common/user.component.php") ?>
        <main>
            <div class="row">
                <div class="col s12 tabs-col">
                    <ul class="tabs">
                        <li class="tab col s6" id="login-tab"><a class="black-text active" href="#login">LOGIN</a></li>
                        <li class="tab col s6" id="register-tab"><a class="black-text example" href="#register">REGISTER</a></li>
                    </ul>
                </div>
                <div id="login" class="col s8 offset-s2">
                    <?php include($COMPONENTS."user/login.component.php"); ?>
                </div>
                <div id="register" class="col s8 offset-s2">
                    <?php include($COMPONENTS."user/register.component.php"); ?>
                </div>
            </div>
        </main>
        <?php require_once($COMPONENTS."common/footer.component.php") ?>

        <script type="text/javascript" src="<?= $ASSETS ?>js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/materialize.min.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/common.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/store.js"></script>    
    </body>
    </html>
<?php endif; ?>