<?php
 $CONFIGS = include("../config.php");
 list($SRC, $COMPONENTS, $ASSETS) = $CONFIGS;
 require($SRC."_bd/bd.php");
 require($SRC."login/Login.php");
 $Login = new Login();
?>

<?php if ($Login->activa() && $Login->getRol() !== 'admin'): ?>
    <?php
        require($SRC."clases/Usuario.php");
        require($SRC."clases/Venta.php");
        require($SRC."clases/Orden.php");
        $bd = new bd();
        $user = new Usuario(intval($Login->get_idUsuario()));
        require($SRC."controllers/user/index.php");
        $user->getUsuario();
        $purchases = $user->getCompras();
    ?>
    <!DOCTYPE html>
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>CORSAIR</title>
        <link rel="icon" href="<?= $ASSETS ?>img/logo_mobile_black.svg">
        <meta name="theme-color" content="#ffffff">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/common.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/store.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
        <?php include($COMPONENTS."common/header.component.php") ?>
        <?php include($COMPONENTS."common/sidemenu.component.php") ?>
        <?php include($COMPONENTS."common/user.component.php") ?>
        <main>
            <div class="row">
                <div class="col s12 center-align"><h4><b>Welcome back <?= $user->nombre ?></b></h4></div>
                <div class="col l6 s12 center align"><br><h5>Check your info</h5>
                    <div class="row">
                        <form action="#" method="post">
                            <input type="hidden" name="form" value="11">
                            <div class="col s4 offset-s2 hoverable">
                                <div class="input-field">
                                    <input value="<?= $user->nombre?>" id="name" name="nombre" type="text" require>
                                    <label for="name">Name</label>
                                </div>
                            </div>
                            <div class="col s4 hoverable">
                                <div class="input-field">
                                    <input value="<?= $user->apellido?>" id="surname" name="apellido" type="text" require>
                                    <label for="surname">Surname</label>
                                </div>
                            </div>
                            <div class="col s8 offset-s2 hoverable">
                                <div class="input-field">
                                    <input value="<?= $user->email ?>" id="email" name="email" type="email" require>
                                    <label for="email">Email</label>
                                </div>
                            </div>
                            <div class="col s8 offset-s2 hoverable">
                                <div class="input-field">
                                    <input value="<?= $user->direccion?>" id="direccion" name="direccion" type="text" require>
                                    <label for="direccion">Address</label>
                                </div>
                            </div>
                            <div class="col s4 offset-s2 hoverable">
                                <div class="input-field">
                                    <input value="<?= $user->ciudad?>" id="city" name="ciudad" type="text" require>
                                    <label for="city">City</label>
                                </div>
                            </div>
                            <div class="col s4 hoverable">
                                <div class="input-field">
                                    <input value="<?= $user->provincia?>" id="state" name="provincia" type="text" require>
                                    <label for="state">State</label>
                                </div>
                            </div>
                            <div class="col s8 offset-s2 center-align">
                                <button class="btn waves-effect waves-light yellow accent-4 black-text" type="submit" name="action"><i class="material-icons left">save</i>SAVE</button>           
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col l6 s12 center align"><br><h5>View your orders</h5>
                    <div class="row">
                        <?php if (count($purchases) > 0) : ?>
                            <?php foreach($purchases as $purchase) : ?>
                                <div class="col s8 offset-s2 z-depth-1 purchase">
                                    <p><?php echo("You spend <b>$".$purchase->total."</b> on the date <b>".$purchase->fecha."</b>"); ?></p>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <div class="col s12 center-align"><h5><b>No orders found...</b></h5></div>
                        <?php endif; ?>
                    </div>
                </div>  
            </div>
        </main>
        <?php include($COMPONENTS."common/login-register.component.php") ?>
        <?php include($COMPONENTS."common/footer.component.php") ?>
        
        <script type="text/javascript" src="<?= $ASSETS ?>js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/materialize.min.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/common.js"></script>
        <script type="text/javascript" src="<?= $ASSETS ?>js/store.js"></script>    
        <?php if ($alert): ?>
            <script>
                M.toast({ html: '<?= $msj ?>' });
            </script>
        <?php endif; ?>  
    </body>
    </html>
<?php else: ?>
    <?php header('Location: ../'); ?>
<?php endif; ?>