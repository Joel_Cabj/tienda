Chart.defaults.global.defaultFontFamily = 'Gotham Light';

var ctx = document.getElementById('totalSells').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: ['Oct-Jan', 'Nov-Feb', 'Dec-March'],
        datasets: [{
            label: 'Q1 2020',
            backgroundColor: 'rgb(255, 214, 0, 0)',
            borderColor: 'rgb(255, 214, 0, 0.7)',
            data: [150, 50, 20]
        },{
            label: 'Q4 2019',
            backgroundColor: 'rgb(35, 31, 32, 0)',
            borderColor: 'rgb(35, 31, 32, 0.7)',
            data: [300, 100, 200]
        }]
    },

    // Configuration options go here
    options: {}
});

var ctx = document.getElementById('byCategory');
var byCategory = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Cases', 'Keyboards', 'Mices', 'Headsets', 'Memory', 'Fans'],
        datasets: [{
            label: '# of sell products per categoty Q4',
            data: [150, 100, 90, 60, 70, 30],
            backgroundColor: [
                'rgba(35, 31, 32, 0.7)',
                'rgba(35, 31, 32, 0.7)',
                'rgba(35, 31, 32, 0.7)',
                'rgba(35, 31, 32, 0.7)',
                'rgba(35, 31, 32, 0.7)',
                'rgba(35, 31, 32, 0.7)'
            ],
            borderColor: [
                'rgba(35, 31, 32, 1)',
                'rgba(35, 31, 32, 1)',
                'rgba(35, 31, 32, 1)',
                'rgba(35, 31, 32, 1)',
                'rgba(35, 31, 32, 1)',
                'rgba(35, 31, 32, 1)'
            ],
            borderWidth: 1
        },{
            label: '# of sell products per categoty Q1',
            data: [70, 60, 20, 35, 5, 30],
            backgroundColor: [
                'rgba(255, 214, 0, 0.7)',
                'rgba(255, 214, 0, 0.7)',
                'rgba(255, 214, 0, 0.7)',
                'rgba(255, 214, 0, 0.7)',
                'rgba(255, 214, 0, 0.7)',
                'rgba(255, 214, 0, 0.7)'
            ],
            borderColor: [
                'rgba(35, 31, 32, 1)',
                'rgba(35, 31, 32, 1)',
                'rgba(35, 31, 32, 1)',
                'rgba(35, 31, 32, 1)',
                'rgba(35, 31, 32, 1)',
                'rgba(35, 31, 32, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});