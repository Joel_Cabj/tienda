$(document).ready(function(){
    $('.tabs').tabs();
    $('.fixed-action-btn').floatingActionButton();
    $('.tooltipped').tooltip();
    $('.modal').modal();
  });


$('#addCategory').on('click', ()=>{
  $("#addCategoryModal").modal('open');
});

$('#addProduct').on('click', () => {
  $("#addProductModal").modal('open');
});

$(document).ready(function(){
  $('select').formSelect();
});

$(document).ready(() => {
  var API = '/tienda/src/controllers/api/index.php';

  $("#productCategorySelect").on('change', (val) => {
    progressBarShow();
    var id_categoria = val.currentTarget.value;

    $.ajax({
      url: API,
      data: { idCategoria: id_categoria },
      dataType: 'json',
      success: (data) => {
        console.log('Subcategorias', data);
        var el = $("#productSubCategorySelect");
        el.empty();
        var options = '';
        subs = data.subCategorias || []; 
        subs.forEach(sc => {
          options += '<option value="' + sc.id_subcategoria + '">' + sc.subcategoria + '</option>';
        });
        if (options === '') {
          options = '<option value="0">Varios</option>';
        }
        el.append(options);
        $('select').formSelect();
        progressBarhide();
      }
    });
  });

  $(".product-view").on('click', (el) => {
    progressBarShow();
    var id = el.currentTarget.getAttribute('data-id');

    $.ajax({
      url: API,
      data: { idProducto: id },
      dataType: 'json',
      success: (data) => {
        console.log('producto', data);
        var producto = data.producto;
        $("#pov-name").text(producto.nombre);
        $("#pov-brand").text(producto.marca);
        var url = '/tienda/assets/img/product/uploads/';
        if (producto.img) {
          url += producto.img;
        } else {
          url += 'no-img.png';
        }
        $("#pov-img").attr('src', url);
        $("#pov-category").text(producto.categoria);
        $("#pov-subCategory").text(producto.subcategoria);
        $("#pov-description").text(producto.descripcion);
        $("#pov-price").text("$"+producto.precio_vta);
        $("#pov-stock").text(producto.stock);
        var urlEdit = "/tienda/admin/edit.php?type=product&id=" + producto.id_producto;
        $("#pov-url-edit").attr('href', urlEdit);
        $('#product-overview').modal('open');
        progressBarhide();
      }
    });
  });

  $(".category-view").on('click', (el) => {
    progressBarShow();
    var id = el.currentTarget.getAttribute('data-id');

    $.ajax({
      url: API,
      data: { idCategoria: id },
      dataType: 'json',
      success: (data) => {
        console.log('producto', data);
        var categoria = data.categoria;
        var subCategoriasHTML = '';
        var subcategorias = data.subCategorias || [];
        subcategorias.forEach(element => {
          subCategoriasHTML += '<tr><td>' + element.subcategoria + '</td><td>' + element.assignedProducts + '</td><td>' + element.hotProducts + '</td> </tr>'; 
        });

        $("#cov-name").text(categoria.categoria);
        
        $('#cov-subcategories').empty();
        if (subCategoriasHTML != '') {
          $('#cov-subcategories').append(subCategoriasHTML);
        } else {
          $('#cov-subcategories').append('<tr><td>Sin Subcategorias</td><td> -- </td><td> -- </td> </tr>')
        }

        var urlEdit = '/tienda/admin/edit.php?type=category&id=' + id;
        console.log('set url', urlEdit);
        
        $("#cov-url-edit").attr('href', urlEdit);
        progressBarhide();
        $('#category-overview').modal('open');
      },
      error: (err) => {
        console.error(err);
      }
    });
  });

  $(".hot-product-btn").on('click', (el) => {
    progressBarShow();
    
    var id = el.currentTarget.getAttribute('data-id');
    var txt = el.currentTarget.innerText === 'star' ? 'star_border' : 'star';
    console.log('id_producto', id);
    $.ajax({
      url: API,
      method: 'POST',
      data: { hotProduct: 1, idProducto: id },
      dataType: 'json',
      success: (data) => {
        console.log('hotProducto', data);
        if (data.ok) {
          el.currentTarget.innerText = txt;
        } else {
          M.toast({ html: 'Error!' });
        }
        progressBarhide();
      }
    });
  });

  $(".pov-hide-btn").on('click', (el) => {
    var id = el.currentTarget.getAttribute('idproduct');
    console.log('idproduct', id);
  });


});