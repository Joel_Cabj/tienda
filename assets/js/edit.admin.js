$(document).ready(() => {
    $(".add-new-subc").on('click', (el) => {
        var id = el.currentTarget.getAttribute('id-categoria');
        $("#addSubCategoryModal").modal('open');
    });

    $(".btn_edit_subcategory").on('click', function() {
        var id_subcategory = $(this).attr('id_subcategory');
        var subcategory_name = $(this).attr('subcategory_name');

        $( "input#id_subcategoria_edit" ).attr("value", id_subcategory);
        $( "input#subcategory_name_edit" ).attr("value", subcategory_name);
        $("#editSubcategoryModal").modal('open');
    });

    $(".btn_remove_subcategory").on('click', function() {
        var id_subcategory = $(this).attr('id_subcategory');
        var subcategory_name = $(this).attr('subcategory_name');

        $( "input#id_subcategoria_remove" ).attr("value", id_subcategory);
        $("b#subcategory_name").html(subcategory_name);
        $("input#no_radio").prop("checked", true);
        $("button.btn_remove_subcategory").attr("disabled", true);

        $("#removeSubcategoryModal").modal('open');
    });

    $("input#yes_radio").on("click", function() {
        $("button.btn_remove_subcategory_submit").attr("disabled", false);
    });

    $("input#no_radio").on("click", function() {
        console.log("Active");
        $("button.btn_remove_subcategory_submit").attr("disabled", true);
    });
})