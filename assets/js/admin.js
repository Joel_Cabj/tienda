$(document).ready(()=>{
    var API = '/tienda/src/controllers/api/index.php';

    $("#productCategorySelect").on('change', (val) => {
        progressBarShow();
        var id_categoria = val.currentTarget.value;

        $.ajax({
            url: API,
            data: { idCategoria: id_categoria},
            dataType: 'json',
            success: (data) => {
                console.log('Subcategorias', data);
                progressBarhide();
            }
        });
    });

});
