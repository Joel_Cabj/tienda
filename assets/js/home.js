
/** COMPONENTES */


/* Img Slider */
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.slider');
    var instances = M.Slider.init(elems, {
        indicators: false,
        height: 700
    });
});  

/* Carousel */
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(elems, {
        dist: 0,
        shift: -20,
        padding: 90,
    });
});

/** OPERADORES **/
