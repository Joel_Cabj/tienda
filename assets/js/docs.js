$(document).ready(function(){

  $('.collection-item').on("click", function() { 
      var id_topic = $(this).attr('id');
      $.ajax({url: `/tienda/docs/topics/${id_topic}.php`, success: function(result){
        $("#topic-title").html(id_topic.toUpperCase());
        $("#content").html(result);
      }});
    });

});