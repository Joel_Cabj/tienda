$(document).ready(()=>{

    $('.add-cart-btn').on('click', (el) => {   
        var precio_vta = el.currentTarget.getAttribute('price');
        var id_producto = el.currentTarget.getAttribute('idProduct');
        var nombre = el.currentTarget.getAttribute('product-name');
        var cantidad = 1;
        var producto = {
            nombre: nombre,
            precio_vta: precio_vta,
            id_producto: id_producto,
            cantidad: cantidad
        };

        var cart = JSON.parse(localStorage.getItem('cart')) || [];
        var filtro = cart.filter(p => p.id_producto === producto.id_producto);
        
        if (filtro.length === 0) {
            cart.push(producto);
        }
        
        M.toast({ html: 'product added!' });
        
        localStorage.setItem('cart', JSON.stringify(cart));
    })

    $('.user-sidenav').on('click', function() {
        $('.cart-section').empty();
        if (!checkStorage()) {
            var subheader = '<li class="center-align"><a class="subheader">Your cart is empty</a></li>';
            var botton = '<li class="center-align"><a href="/tienda/store"><i class="material-icons right">shopping_cart</i>Lets go shopping</a></li>';
            
            $('.cart-section').append(subheader, botton);
        } else {
            var cartItems = JSON.parse(localStorage.getItem("cart"));
            var divider = '<li><div class="divider sidenav-divider"></div></li>';
            var botton = '<li class="center-align"><a href="/tienda/store/checkout.php#confirm-products"><i class="material-icons right">payment</i>Checkout</a></li>';

            for(var i = 0; i < cartItems.length; i++) {
                var cartItem = cartItems[i];
                $('.cart-section').append(`<li class="center-align">${cartItem.nombre} - ${cartItem.precio_vta}</li>`);
            }

            $('.cart-section').append(divider, botton);
        }
    })

    if ($('.check-cart-section').length) {
        if (!checkStorage()) {
            console.error("Storage is not set");
            $('.check-cart-section').append('<div class="col s6 offset-s3 center-align"><h4>Thanks! click <a href="/tienda/user/">here</a> for checkin your order</h4></div>');
            $('.sub_total_price_col').remove();
            $("a.btn_next").attr("disabled", true);
        } else {
            var cart = JSON.parse(localStorage.getItem("cart"));

            for(var i = 0; i < cart.length; i++) {
                var cartItem = cart[i];
                product_price = Math.round(parseInt(cartItem.precio_vta));
                
                $('.check-cart-section').append(
                    `<div class="col s6 offset-s3 hoverable product-check-cart id_product_${cartItem.id_producto}" style="margin-bottom: 10px;">
                        <div class="col s6 left-align">
                            <h5>${cartItem.nombre}</h5>
                        </div>
                        <div id_product="${cartItem.id_producto}" class="col s6 right-align clear_product">
                            <a href="#"><i class="material-icons grey-text clear-btn">clear</i></a>
                        </div>
                        <div class="col s12">
                            <p class="range-field">    
                                <input class="quantity_field" type="range" id_product="${cartItem.id_producto}" value="${cartItem.cantidad}" price="${Math.round(cartItem.precio_vta)}" min="1" max="10"/>
                                <span class="helper-text grey-text">Selected Quantity: <b class="product_quantity_${cartItem.id_producto}">${cartItem.cantidad}</b></span>
                                <span class="helper-text grey-text">/ Price: $<b class="product_price_${cartItem.id_producto}">${product_price}</b></span>
                            </p>
                        </div>
                    </div>`);
            }
        }
    }

    if ($('.cart-overview-section').length) {
        if (!checkStorage()) {
            console.error("Storage is not set");
            $("button.btn_confirm").prop("disabled", true);
            $('.cart-overview-section').append('<div class="col s6 offset-s3 center-align"><h4>Something went wrong... click <a href="/tienda/store/">here<a></h4></div>');
        } else {
            var cart = JSON.parse(localStorage.getItem("cart"));
            var total_price = 0;
            var total_items = 0;

            for(var i = 0; i < cart.length; i++) {
                var cartItem = cart[i];
                total_items += cartItem.cantidad;
                total_price += Math.round(cartItem.precio_vta * cartItem.cantidad);

                $('.cart-overview-section').append(
                    `<div class="col s8 offset-s2 product-cart-overview z-depth-1 id_product_${cartItem.id_producto}">
                        <input type="hidden" id="id_product" name="id_producto" value="${cartItem.id_producto}">
                        <div class="col s6 left-align">
                            <h5><b>${cartItem.nombre}</b></h5>
                        </div>
                        <div class="col s6 right-align">
                            <span class="helper-text grey-text">Quantity: <b class="product_quantity_${cartItem.id_producto}">${cartItem.cantidad}</b></span><br>
                            <span class="helper-text grey-text">Price: $<b class="product_price_${cartItem.id_producto}">${Math.round(total_price)}</b></span><br>
                        </div>
                    </div>`);
            }

            $("b.sub_total_price").html(`$${total_price}`);
            $("b.total_price").html(`$${total_price}`);
            $("b.items_quantity").html(total_items);
        }

        $("#checkout_products_list").val(JSON.stringify(cart));
    }

    $('.clear_product').on('click', function() {
        var cart = JSON.parse(localStorage.getItem("cart"));
        var id_product = $(this).attr('id_product');
        const newCart = cart.filter((item) => item.id_producto !== id_product);

        localStorage.removeItem('cart');

        if (newCart === null || newCart.length === 0) {
            console.error("Array is empty or null");
            $('.check-cart-section').append('<div class="col s6 offset-s3 center-align"><h4>Go back to the <a href="/tienda/store/">Store<a></h4></div>');
            $('.sub_total_price_col').remove();
            $("a.btn_next").attr("disabled", true);
        } else {
            localStorage.setItem('cart', JSON.stringify(newCart));
        }

        $(`div.id_product_${id_product}`).remove();
    })

    // Update cart when product quantity changes.
    $('.quantity_field').on('change', function() {
        var cart = JSON.parse(localStorage.getItem("cart"));
        var total_price = 0;
        var quantity = $(this).val();
        var id_producto = $(this).attr('id_product');
        var precio_vta = $(this).attr('price');
        var total_precio = quantity * precio_vta;
        total_price += Math.round(total_precio); 
        
        cart.forEach(product => {
            if (parseInt(product.id_producto) === parseInt(id_producto)) {
                product.cantidad = parseInt(quantity);
            } else {
                total_price += Math.round(product.cantidad * product.precio_vta);
            }
        });

        $("b.sub_total_price").html(`$${total_price}`);
        $("b.total_price").html(`$${total_price}`);
        $(`b.product_quantity_${id_producto}`).html(quantity);

        localStorage.setItem('cart', JSON.stringify(cart));
        $("#checkout_products_list").val(JSON.stringify(cart));
    });

    $("a.btn_next").on("click", function() {
        $("li#confirm-tab").removeClass("disabled");
        $('ul.tabs').tabs('select', 'confirm');
        $("li#check-cart-tab").addClass("disabled");
    });

    $("a.btn_back").on("click", function() {
        $("li#check-cart-tab").removeClass("disabled");
        $('ul.tabs').tabs('select', 'check-cart');
        $("li#confirm-tab").addClass("disabled");
    });

    $("a.btn_register").on("click", function() {
        $('ul.tabs').tabs('select', 'register');
    });

    $("a.btn_login").on("click", function() {
        $('ul.tabs').tabs('select', 'login');
    });

    function checkStorage() {
        if(!localStorage.getItem('cart')) {
            return false;
          } else {
            return true;
          }
    }
})