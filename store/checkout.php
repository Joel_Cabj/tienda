<?php
 $CONFIGS = include("../config.php");
 list($SRC, $COMPONENTS, $ASSETS) = $CONFIGS;
 require($SRC."_bd/bd.php");
 require_once($SRC."login/Login.php");
 $Login = new Login();
?>

<?php if ($Login->getRol() === 'admin'): ?>
    <?php header('Location: ../admin/'); ?>
<?php else: ?>
	<?php if ($Login->activa()) : ?>
        <?php 
        require($SRC."clases/Usuario.php");
        require($SRC."clases/Venta.php");
        require($SRC."clases/Orden.php");
        $bd = new bd();
        $user = new Usuario(intval($Login->get_idUsuario()));
        $user->getUsuario();
        require_once($SRC."controllers/store/checkout/index.php"); 
        ?>
        <!DOCTYPE html>
        <html lang="es">
            <head>
                <meta charset="UTF-8">
                <title>CORSAIR | CHECKOUT</title>
                <link rel="icon" href="<?= $ASSETS ?>img/logo_mobile_black.svg">
                <meta name="theme-color" content="#ffffff">
                <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
                <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/materialize.min.css"  media="screen,projection"/>
                <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/common.css"  media="screen,projection"/>
                <link type="text/css" rel="stylesheet" href="<?= $ASSETS ?>css/store.css"  media="screen,projection"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            </head>

            <body>
                <?php require_once($COMPONENTS."common/header.component.php") ?>
                <?php require_once($COMPONENTS."common/sidemenu.component.php") ?>
                <?php require_once($COMPONENTS."common/user.component.php") ?>
                <?php require_once($COMPONENTS."common/login-register.component.php") ?>
                <main>
                    <div class="row">
                        <div class="col s12 tabs-col">
                            <ul class="tabs tabs-fixed-width">
                                <li class="tab col s6" id="check-cart-tab"><a class="black-text active" href="#check-cart">CHECK CART</a></li>
                                <li class="tab col s6 disabled" id="confirm-tab"><a class="black-text example" href="#confirm">CONFIRM</a></li>
                            </ul>
                        </div>
                        <form action="#" method="POST">
                            <input type="hidden" id="checkout_products_list" name="products" value="{}">
                            <div id="check-cart" class="col s12">
                                <?php include($COMPONENTS."store/checkout-check-cart.component.php"); ?>
                            </div>
                            <div id="confirm" class="col s12">
                                <?php include($COMPONENTS."store/checkout-confirm.component.php"); ?>
                            </div>
                        </form> 
                    </div>
                </main>
                <?php require_once($COMPONENTS."common/footer.component.php") ?>

                <script type="text/javascript" src="<?= $ASSETS ?>js/jquery-2.1.1.min.js"></script>
                <script type="text/javascript" src="<?= $ASSETS ?>js/materialize.min.js"></script>
                <script type="text/javascript" src="<?= $ASSETS ?>js/common.js"></script>
                <?php if ($alert): ?>
                    <script>
                        M.toast({ html: '<?= $msj ?>' });
                        localStorage.removeItem('cart');
                    </script>
                <?php endif; ?>
                <script type="text/javascript" src="<?= $ASSETS ?>js/store.js"></script>      
            </body>
        </html>
	<?php else: ?>
		<?php header('Location: ../user/validate.php'); ?>	
	<?php endif; ?>
<?php endif; ?>